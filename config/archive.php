<?php

return [
    
    'enquirystatus' => [
        "1" => 'OPEN',
        "2" => 'PROCESSING',
        "3" => 'CLOSED',
        // "4" => 'REJECTED',
    ],
];

// 'enquirystatus' => [
//     "1" => 'OPEN',
//     "2" => 'PROCESSING',
//     "3" => 'SOLVED',
//     "4" => 'REJECTED',
// ],
