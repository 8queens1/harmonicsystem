<?php

use App\Http\Controllers\Website\Blog\BlogController;
use App\Http\Controllers\Website\WebsiteController;
use Illuminate\Support\Facades\Route;

Route::get('/search', [BlogController::class, 'search'])->name('search');
Route::post('/searchpost', [BlogController::class, 'searchpost'])->name('searchpost');

// Route::get('/', [BlogController::class, 'blog'])->name('blog');
Route::get('/index', [BlogController::class, 'blog']);
Route::get('/blog', [BlogController::class, 'blog']);
Route::get('/blog/category', [BlogController::class, 'categoryList']);
Route::get('/blog/tag', [BlogController::class, 'tagList']);
Route::get('/blog/{slug}', [BlogController::class, 'article']);
Route::get('/blog/category/{id}', [BlogController::class, 'category'])->name('category');
Route::get('/blog/subcategory/{subcategory}/{category}', [BlogController::class, 'subcategory'])->name('subcategory');
Route::get('/blog/tag/{id}/{slug}', [BlogController::class, 'tag'])->name('tag');
Route::post('/newsletter', [BlogController::class, 'newsletter'])->name('newsletter');

Route::get('/page/{slug}', [WebsiteController::class, 'page'])->name('page');
Route::post('/contactus', [WebsiteController::class, 'contactus'])->name('contactus');

// Website Pages
Route::get('/', [WebsiteController::class, 'home'])->name('home');
Route::get('/home', [WebsiteController::class, 'home'])->name('home');
Route::get('/aboutus', [WebsiteController::class, 'aboutus'])->name('aboutus');
Route::get('/service', [WebsiteController::class, 'service'])->name('service');
Route::get('/product', [WebsiteController::class, 'product'])->name('product');
Route::get('/whyus', [WebsiteController::class, 'whyus'])->name('whyus');
Route::get('/contact', [WebsiteController::class, 'contact'])->name('contact');
