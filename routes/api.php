<?php

use Illuminate\Http\Request;
use App\Models\Admin\Blog\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BlogapiController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/allblog', [BlogapiController::class, 'allblog']);
Route::get('/allblogtwo', [BlogapiController::class, 'allblogtwo']);
