<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Blog\TagController;
use App\Http\Controllers\Admin\Blog\PostController;
use App\Http\Controllers\Admin\Blog\AboutmeController;
use App\Http\Controllers\Admin\Blog\CategoryController;
use App\Http\Controllers\Admin\Website\EnquiryController;
use App\Http\Controllers\Admin\Website\MarqueeController;
use App\Http\Controllers\Admin\Blog\SubcategoryController;
use App\Http\Controllers\Admin\Settings\SettingsController;
use App\Http\Controllers\Admin\Blog\NewsletteruserController;
use App\Http\Controllers\Admin\Dashboard\DashboardController;
use App\Http\Controllers\Admin\Settings\AddemployeeController;
use App\Http\Controllers\Admin\Website\WebsitepagesController;
use App\Http\Controllers\Admin\Settings\ConfigurationController;
use App\Http\Controllers\Admin\Officeutility\EventcalendarController;
use App\Http\Controllers\Admin\Configuration\Google2fa\PasswordSecurityController;

Auth::routes();


Route::post('/2faVerify', function () {
    return redirect(URL()->previous());
})->name('2faVerify')->middleware('2fa');

Route::get('decompose','\Lubusin\Decomposer\Controllers\DecomposerController@index')->name('decompose')->middleware('auth', 'preventbackbutton');
// '2fa',
Route::group(['middleware' => ['auth', 'preventbackbutton'], 'prefix' => 'admin'], function () {
    // Dashboard
    Route::get('/admindashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
    Route::get('/loginlogs', [DashboardController::class, 'loginlogs'])->name('loginlogs');
    Route::get('/trackinglogs', [DashboardController::class, 'trackinglogs'])->name('trackinglogs');

    Route::resources([
        'marquee' => MarqueeController::class,
        'enquiry' => EnquiryController::class,
        'eventcalendar' => EventcalendarController::class,
        //Blog
        'post' => PostController::class,
        'category' => CategoryController::class,
        'subcategory' => SubcategoryController::class,
        'tag' => TagController::class,
        'newsletteruser' => NewsletteruserController::class,
        'aboutme' => AboutmeController::class,
        'websitepages' => WebsitepagesController::class,

        'configuration' => ConfigurationController::class,
        'addemployee' => AddemployeeController::class,
    ]);

    Route::post('/ajaxpost', [PostController::class, 'ajaxpost'])->name('ajaxpost');
    Route::get('/ajaxtagcategory', [PostController::class, 'ajaxtagcategory'])->name('ajaxtagcategory');
    Route::post('/postimg/deletegal', [PostController::class, 'deletegal'])->name('postimg.deletegal');
    Route::get('postimg/fetch', [PostController::class, 'fetch'])->name('postimg.fetch');

    Route::get('ajaxsubcategory', [CategoryController::class, 'ajaxsubcategory'])->name('ajaxsubcategory');
    Route::get('getSubcategroyList', [PostController::class, 'getSubcategroyList'])->name('getSubcategroyList');
    // State List
    Route::get('/getStateList', [PostController::class, 'getStateList'])->name('getStateList');
    Route::get('/getCityList', [PostController::class, 'getCityList'])->name('getCityList');
    // Newsletters
    Route::get('newsletteruserstatus/{newsletteruser}/{status}', [NewsletteruserController::class, 'newsletteruserstatus'])->name('newsletteruserstatus');

    // Configuration
    // Settings
    Route::get('/settings', [SettingsController::class, 'index'])->name('settings');
    // System Info
    Route::get('/systeminfo', [SettingsController::class, 'systeminfo'])->name('systeminfo');
    Route::get('/cacheclear', [SettingsController::class, 'cacheclear'])->name('cacheclear');

    // Add Employee //
    Route::get('/ajaxaddemployee', [AddemployeeController::class, 'ajaxaddemployee'])->name('ajaxaddemployee');
    Route::get('/profile', [AddemployeeController::class, 'profile'])->name('profile');
    Route::get('/changepasswordform', [AddemployeeController::class, 'changepasswordform'])->name('changepasswordform');
    Route::post('/changepassword', [AddemployeeController::class, 'changepassword'])->name('changepassword');

    Route::get('/2fa', [PasswordSecurityController::class, 'show2faForm'])->name('2fa');
    Route::post('/generate2faSecret', [PasswordSecurityController::class, 'generate2faSecret'])->name('generate2faSecret');
    Route::post('/2fa', [PasswordSecurityController::class, 'enable2fa'])->name('enable2fa');
    Route::post('/disable2fa', [PasswordSecurityController::class, 'disable2fa'])->name('disable2fa');

});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::get('/backuprun', [SettingsController::class, 'backuprun'])->name('backuprun');
});
