<?php

namespace App\View\Components\Admin\Layout;

use Illuminate\View\Component;

class indexnav extends Component
{
    public $name;
    public $title;
    public $button;
    public $gate;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $title, $button, $gate)
    {
        $this->name    = $name;
        $this->title   = $title;
        $this->button  = $button;
        $this->gate    = $gate;
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin.layout.indexnav');
    }
}
