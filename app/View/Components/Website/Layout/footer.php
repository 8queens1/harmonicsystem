<?php

namespace App\View\Components\Website\Layout;

use Illuminate\View\Component;
use App\Models\Admin\Blog\Category;

class footer extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        // $categories = Category::where('status', 1)->inRandomOrder()->take(10)->get();
        // return view('components.website.layout.footer', compact('categories'));
        return view('components.website.layout.footer');
    }
}
