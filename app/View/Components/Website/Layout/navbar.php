<?php

namespace App\View\Components\Website\Layout;

use Illuminate\View\Component;
use App\Models\Admin\Website\Websitepages;

class navbar extends Component
{

  //  public $navlist;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->navlist = Websitepages::where('active', 1)->get();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        //$navlist = Websitepages::where('active', 1)->get();
        return view('components.website.layout.navbar');
    }
}
