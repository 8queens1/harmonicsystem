<?php

namespace App\Models\Admin\Blog;

use Illuminate\Database\Eloquent\Model;

class Aboutme extends Model
{
    protected $dates = ['deleted_at'];
    protected $guarded = ['id'];
}
