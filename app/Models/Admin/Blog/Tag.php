<?php

namespace App\Models\Admin\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Tag extends Model
{
    use SoftDeletes;

    protected $dates   = ['deleted_at'];
    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Admin\Blog\Post', 'post_tags')->orderBy('created_at', 'DESC')->paginate(5);
    }

    public function tagposts()
    {
        return $this->belongsToMany('App\Models\Admin\Blog\Post', 'post_tags');
    }

}
