<?php

namespace App\Models\Admin\Blog;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Illuminate\Support\Str;

class Post extends Model
{
    use Searchable;
    use SoftDeletes;

    protected $dates   = ['deleted_at'];
    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Admin\Blog\Tag', 'post_tags')->withTimestamps();
    }

    public function category() {
        return $this->belongsTo('App\Models\Admin\Blog\Category');
    }

    public function subcategory() {
        return $this->belongsTo('App\Models\Admin\Blog\Subcategory');
    }



    public function getTagSelectAttribute()
    {
        return $this->tags->pluck('id');
    }

    // public function getCategorySelectAttribute()
    // {
    //     return $this->categories->pluck('id');
    // }

    // public function getCreatedAtAttribute($value)
    // {
    //     return Carbon::parse($value)->diffForHumans();
    // }

    public function categorytwo() {
        return $this->belongsTo('App\Models\Admin\Blog\Categorytwo');
    }
    
}
