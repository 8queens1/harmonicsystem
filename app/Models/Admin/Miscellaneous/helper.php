<?php

namespace App\Models\Admin\Miscellaneous;

use App\Models\Admin\Miscellaneous\tracking;
use Auth;
use Illuminate\Database\Eloquent\Model;

class helper extends Model
{
    public static function getNextSequenceId($digit, $name, $model)
    {
        $object = $model::withTrashed()
            ->orderBy('created_at', 'desc')
            ->orderBy('sequence_id', 'desc')
            ->first();

        $lastId = (!$object) ? 0 : $object->sequence_id;

        return $insertDataTwo = array(
            'uniqid'      => $name . '-' . sprintf('%0' . $digit . 'd', intval($lastId) + 1),
            'sequence_id' => $lastId + 1,
            'sys_id'      => md5(uniqid(rand(), true)),
        );
    }

    public static function trackmessage($trackmsg, $panel)
    {
        tracking::create(['details' => $trackmsg,
            'name'                      => Auth::user()->name,
            'user_id'                   => Auth::user()->id,
            'uuid'                      => Auth::user()->uuid,
            'panal'                     => $panel,
        ]);
    }
}
