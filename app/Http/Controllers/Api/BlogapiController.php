<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Admin\Blog\Post;
use App\Http\Controllers\Controller;

class BlogapiController extends Controller
{
    public function allblog()
    {
     return   $post = Post::where('active', 1)->where('publish', 1)->with('tags')->orderBy('created_at', 'desc')->get();
    }

    public function allblogtwo()
    {
      return  $post = Post::where('active', 1)->where('publish', 1)->with('tags')->orderBy('created_at', 'desc')->paginate(2);
    }
}
