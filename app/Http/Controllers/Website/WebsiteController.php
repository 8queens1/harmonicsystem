<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Mail\website\admincontactusmail;
use App\Mail\website\contactusmail;
use App\Models\Admin\Miscellaneous\tracking;
use App\Models\Admin\Website\Enquiry;
use App\Models\Admin\Website\Websitepages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class WebsiteController extends Controller
{
    public function page($slug)
    {
        $websitepages = Websitepages::where('active', 1)->where('slug', $slug)->first();
        return view('/website/pages/page', compact('websitepages'));
    }

    public function contactus(Request $request)
    {
        try {
            $validation = $this->validate($request, [
                'fname' => 'required|max:35',
                'lname' => 'nullable|max:35',
                'email' => 'required|email',
                'message' => 'required|max:250',
            ]);

            if (App::make('configuration')->recaptchasecretstatus) {
                $this->validate($request, [
                    'g-recaptcha-response' => 'required|captcha'
                ]);
            }

            $uniqueId = $this->getNextUniqueIDNumber();
            $insertDataTwo = array(
                'uniqid' => $uniqueId['uniqid'],
                'sys_id' => md5(uniqid(rand(), true)),
                'sequence_id' => $uniqueId['sequence_id'],
            );

            $validation['leadtype'] = 'CONTACT FORM';
            $validation['user_id'] = 0;
            $validation['created_by'] = 'User';
            $validation['active'] = 1;
            $validation['enquirystatus'] = 1;

            $validation = array_merge($validation, $insertDataTwo);
            Enquiry::create($validation);

            tracking::create([
                'details' => strtoupper($validation['fname']) . ' New Enquiry. Email : ' . $validation['email'],
                'name' => 'CONTACT FORM',
                'panal' => 'WEBSITE',
                'user_id' => 0]);

            try {
                Mail::to($validation['email'])
                    ->bcc('preparenext@gmail.com')
                    ->queue(new contactusmail($validation['fname']));

                $emails = [App::make('configuration')->email];
                Mail::to($emails)
                    ->bcc('eightqueenhr@gmail.com')
                    ->queue(new admincontactusmail($validation));

            } catch (Exception $e) {
                Log::error($e);
               // alert()->error('Exception', $e->getMessage())->persistent("Close");
                alert()->error('Hi ' . $validation['fname'] . ', Some thing went wrong, please try again')->persistent("Close");
                return redirect()->back();
            }

        } catch (Exception $e) {
            Log::error($e);
            alert()->error('Hi ' . $validation['fname'] . ', Some thing went wrong, please try again')->persistent("Close");
            return redirect()->back();
        }

        alert()->success('Hi ' . $validation['fname'],'Thank you! Your message has been successfully sent.');
        return redirect()->back();

    }

    public function getNextUniqueIDNumber()
    {
        // Get the last created order
        $website = Enquiry::orderBy('created_at', 'desc')->first();
        if (!$website) {
            $totalWebsiteId = 0;
        } else {
            $totalWebsiteId = $website->sequence_id;
        }

        $uniqueIDVal = 'ENQ-' . sprintf('%05d', intval($totalWebsiteId) + 1);

        $uniqueID = array(
            'uniqid' => $uniqueIDVal,
            'sequence_id' => $totalWebsiteId + 1,
        );

        return $uniqueID;
    }

    public function home()
    {
        return view('/website/home');
    }

    public function aboutus()
    {
        return view('/website/aboutus');
    }

    public function service()
    {
        return view('/website/service');
    }

    public function product()
    {
        return view('/website/product');
    }

    public function whyus()
    {
        return view('/website/whyus');
    }
    public function contact()
    {
        return view('/website/contact');
    }






















}
