<?php

namespace App\Http\Controllers\Website\Blog;

use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Aboutme;
use App\Models\Admin\Blog\Category;
use App\Models\Admin\Blog\Newsletteruser;
use App\Models\Admin\Blog\Post;
use App\Models\Admin\Blog\Tag;
use App\Models\Admin\Miscellaneous\helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Newsletter;

class BlogController extends Controller
{

    public function search(Request $request)
    {
        if ($request->ajax()) {

            if (strlen($request->searchlist) > 2) {
                if (App::make('configuration')->searchstatus == 2) {
                    $data = Post::search(trim($request->searchlist))->get();
                } else {
                    $data = Post::where('title', 'LIKE', '%' . trim($request->searchlist) . '%')->limit(10)->get();
                }
            } else {
                return $output = '';
            }
            $output = '';
            if (count($data) > 0) {
                $output = '<ul class="absolute rounded md:w-96">';
                foreach ($data as $row) {
                    $output .= '<li class="border-b-2 border-gray-100 rounded text-white bg-gray-500 hover:bg-gray-600 py-2 px-4 block whitespace-no-wrap"><a href="#">' . $row->title . '</a></li>';
                }
                $output .= '</ul>';
            } else {
                $output = '<ul class="absolute p-3 rounded">';
                $output .= '<li class="rounded text-white bg-gray-500 hover:bg-gray-600 py-2 px-4  whitespace-no-wrap">' . 'No results' . '</li>';
                $output .= '</ul>';
            }
            return $output;
        }
    }

    public function searchpost(Request $request)
    {
        $aboutme = Aboutme::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        $categories = collect(Category::where('status', 1)->with('subcategories')->get());
        $tags = Tag::where('status', 1)->inRandomOrder()->take(10)->get();
        $post = Post::where('active', 1)->where('publish', 1)->where('title', $request->searchlist)->first();
        return view('website.blog.article', compact('post', 'tags', 'categories', 'aboutme'));
    }

    public function blog()
    {
        $aboutme = Aboutme::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        $categories = collect(Category::where('status', 1)->with('subcategories')->get());
        $tags = Tag::where('status', 1)->inRandomOrder()->take(10)->get();
        $post = Post::where('active', 1)->where('publish', 1)->with('tags')->orderBy('created_at', 'desc')->paginate(8);

        switch (App::make('configuration')->blogtemplates) {
            case 0:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            case 1:
                return view('website.blog.indextwo', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            default:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
        }

    }

    public function article(Request $request, $slug)
    {
        $aboutme = Aboutme::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        $categories = collect(Category::where('status', 1)->with('subcategories')->get());
        $tags = Tag::where('status', 1)->inRandomOrder()->take(10)->get();
        $post = Post::where('active', 1)->where('publish', 1)->where('postslug', $slug)->first();
        return view('website.blog.article', compact('post', 'tags', 'categories', 'aboutme'));
    }

    public function category(Request $request, $category)
    {
        $post = Post::where('active', 1)->where('category_id', $category)->where('publish', 1)->with('tags')->orderBy('created_at', 'desc')->paginate(8);
        $categories = collect(Category::where('status', 1)->with('subcategories')->get());
        $aboutme = Aboutme::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        $tags = Tag::where('status', 1)->inRandomOrder()->take(10)->get();

        switch (App::make('configuration')->blogtemplates) {
            case 0:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            case 1:
                return view('website.blog.indextwo', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            default:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
        }
    }

    public function subcategory(Request $request, $subcategory, $category)
    {
        $post = Post::where('active', 1)->where('category_id', $category)->where('subcategory_id', $subcategory)->where('publish', 1)->with('tags')->orderBy('created_at', 'desc')->paginate(8);
        $categories = collect(Category::where('status', 1)->with('subcategories')->get());
        $aboutme = Aboutme::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        $tags = Tag::where('status', 1)->inRandomOrder()->take(10)->get();

        switch (App::make('configuration')->blogtemplates) {
            case 0:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            case 1:
                return view('website.blog.indextwo', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            default:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
        }
    }

    public function tag(Request $request, $id, $slug)
    {
        $tag = Tag::find($id);
        $post = isset($tag) ? $tag->posts() : [];
        $aboutme = Aboutme::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        $categories = collect(Category::where('status', 1)->with('subcategories')->get());
        $tags = Tag::where('status', 1)->inRandomOrder()->take(10)->get();

        switch (App::make('configuration')->blogtemplates) {
            case 0:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            case 1:
                return view('website.blog.indextwo', compact('post', 'tags', 'categories', 'aboutme'));
                break;
            default:
                return view('website.blog.index', compact('post', 'tags', 'categories', 'aboutme'));
        }
    }

    public function categorylist()
    {
        $categories = Category::where('status', 1)->orderBy('created_at', 'desc')->get();
        return view('website/blog/category', compact('categories'));
    }

    public function taglist()
    {
        $tags = Tag::where('status', 1)->orderBy('created_at', 'desc')->get();
        return view('website/blog/tag', compact('tags'));
    }
    public function newsletter(Request $request)
    {
        try {
            $validation = $this->validate($request, [
                'mail' => 'required|email|unique:newsletterusers',
            ]);

            $uniqueId = helper::getNextSequenceId(5, 'NL', 'App\Models\Admin\Blog\Newsletteruser');

            Newsletteruser::create([
                'mail' => $request->mail,
                'newsletter_status' => 1,
                'active' => 1,
                'sys_id' => md5(uniqid(rand(), true)),
                'uniqid' => $uniqueId['uniqid'],
                'sequence_id' => $uniqueId['sequence_id'],
            ]);

            if (!Newsletter::isSubscribed($request->mail)) {
                Newsletter::subscribe($request->mail);
                alert()->success('Thank you!', 'You have successfully subscribed to the newsletter.');
            }
            return redirect()->back();
        } catch (Exception $e) {
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }
}
