<?php

namespace App\Http\Controllers\Admin\Blog;

use DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Admin\Blog\Category;
use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Subcategory;
use App\Models\Admin\Miscellaneous\helper;
use App\Models\Admin\Miscellaneous\tracking;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $category = category::select(array('id', 'uniqid', 'name', 'created_by', 'created_at'));
            return DataTables::of($category)
                ->addColumn('action', function ($category) {
                    return '<td class="text-right">
                    <a href="category/' . $category->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="category/' . $category->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin/blog/category/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(category $category)
    {
        return view('/admin/blog/category/createorupdate', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $validation = $this->validate($request, [
                'name'        => 'required|max:100',
                'slug'        => 'required',
                'keyword'     => 'required',
                'description' => 'required',
                'comments'    => 'nullable',
            ]);


          //  return $request->category_select;
            $this->createorupdate($validation, $request);

            DB::commit();
            return redirect()->route('category.index');

        } catch (Exception $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }

    public function createorupdate($validation, $request)
    {
        $validation['active']     = $request->has('active');
        $validation['status']     = 1;
        $validation['slug']       = Str::slug($validation['slug'], '-');
        $validation['user_id']    = Auth::user()->id;
        $validation['created_by'] = Auth::user()->name;

        if (!empty($request['id'])) {
            $validation['uniqid']      = $request['uniqid'];
            $category = category::where('id', $request['id'])->update($validation);
            $category = category::find($request['id']);
            $category->subcategories()->sync($request->category_select);
            toast('Category Updated successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Updated Existing Category';
        } else {
            $uniqueId                  = helper::getNextSequenceId(5, 'CATEGORY', 'App\Models\Admin\Blog\Category');
            $validation['sys_id']      = md5(uniqid(rand(), true));
            $validation['uniqid']      = $uniqueId['uniqid'];
            $validation['sequence_id'] = $uniqueId['sequence_id'];
            $category =category::create($validation);
            $category->subcategories()->attach($request->category_select);
            toast('New Category Created Successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Created New Category';
        }

        tracking::create(['details' => $trackStatus,
            'name'                      => Auth::user()->name,
            'user_id'                   => Auth::user()->id,
            'uuid'                      => Auth::user()->uuid,
            'panal'                     => 'ADMIN',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Model\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function show(category $category)
    {
        //return $category->subcategories;
        return view('/admin/blog/category/show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Model\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function edit(category $category)
    {
        return view('/admin/blog/category/createorupdate', compact('category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Model\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(category $category)
    {
        // $category->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('category.index');
    }

    public function ajaxsubcategory()
    {
        $category       = Subcategory::all();
        $categoryoption = '<option value="">SELECT SUB CATEGORY</option>';
        foreach ($category as $row) {
            $categoryoption .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        $output = array(
            'categoryoption' => $categoryoption,
        );

        echo json_encode($output);
    }

}
