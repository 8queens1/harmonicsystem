<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Subcategory;
use App\Models\Admin\Miscellaneous\helper;
use App\Models\Admin\Miscellaneous\tracking;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class SubcategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $category = Subcategory::select(array('id', 'uniqid', 'name', 'created_by', 'created_at'));
            return DataTables::of($category)
                ->addColumn('action', function ($category) {
                    return '<td class="text-right">
                    <a href="subcategory/' . $category->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="subcategory/' . $category->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin/blog/subcategory/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(subcategory $subcategory)
    {
        return view('/admin/blog/subcategory/createorupdate', compact('subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $validation = $this->validate($request, [
                'name'        => 'required|max:100',
                'slug'        => 'required',
                'keyword'     => 'required',
                'description' => 'required',
                'comments'    => 'nullable',
            ]);

            $this->createorupdate($validation, $request);

            DB::commit();
            return redirect()->route('subcategory.index');

        } catch (Exception $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }

    public function createorupdate($validation, $request)
    {
        $validation['active']     = $request->has('active');
        $validation['status']     = 1;
        $validation['slug']       = Str::slug($validation['slug'], '-');
        $validation['user_id']    = Auth::user()->id;
        $validation['created_by'] = Auth::user()->name;

        if (!empty($request['id'])) {
            $validation['uniqid']      = $request['uniqid'];;
            Subcategory::where('id', $request['id'])->update($validation);
            toast('Sub Category Updated successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Updated Existing Sub Category';
        } else {
            $uniqueId                  = helper::getNextSequenceId(5, 'SUBCAT', 'App\Models\Admin\Blog\Subcategory');
            $validation['sys_id']      = md5(uniqid(rand(), true));
            $validation['uniqid']      = $uniqueId['uniqid'];
            $validation['sequence_id'] = $uniqueId['sequence_id'];
            Subcategory::create($validation);
            toast('New Sub Category Created Successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Created New Sub Category';
        }

        tracking::create(['details' => $trackStatus,
            'name'                      => Auth::user()->name,
            'user_id'                   => Auth::user()->id,
            'uuid'                      => Auth::user()->uuid,
            'panal'                     => 'ADMIN',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Model\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function show(subcategory $subcategory)
    {
        return view('/admin/blog/subcategory/show', compact('subcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Model\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function edit(subcategory $subcategory)
    {
        return view('/admin/blog/subcategory/createorupdate', compact('subcategory'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Model\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(subcategory $subcategory)
    {
        // $category->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('subcategory.index');
    }

}
