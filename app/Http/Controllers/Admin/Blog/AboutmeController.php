<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Aboutme;
use App\Models\Admin\Miscellaneous\tracking;
use Auth;
use DB;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AboutmeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutme = Aboutme::where('uuid', '88fc5a4d-8283-478b-aba2-8queens')->first();
        return view('/admin/blog/aboutme/createOrUpdate', compact('aboutme'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation = $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'active' => 'nullable',
                'avataractive' => 'nullable',
                'avatartaglineone' => 'nullable',
                'avatartaglinetwo' => 'nullable',
            ]);

            $validation['active'] = !$request['active'] ? 0 : 1;
            $validation['avataractive'] = !$request['avataractive'] ? 0 : 1;

            if ($request->file('avatar')) {
                $this->validate($request, [
                    'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $avatar = Auth::user()->avatar;
                if ($avatar) {
                    $thumbnail = public_path("avatar/thumbnail/{$avatar}");
                    $images = public_path("avatar/images/{$avatar}");

                    if (File::exists($thumbnail)) {
                        unlink($thumbnail);
                        unlink($images);
                    }
                }

                $originalImage = $request->file('avatar');
                $thumbnailImage = Image::make($originalImage);
                $thumbnailPath = public_path() . '/avatar/thumbnail/';
                $originalPath = public_path() . '/avatar/images/';
                $thumbnailImage->save($originalPath . time() . $originalImage->getClientOriginalName());
                $thumbnailImage->resize(150, 150);
                $thumbnailImage->save($thumbnailPath . time() . $originalImage->getClientOriginalName());
                $validation['avatar'] = time() . $originalImage->getClientOriginalName();

            }

            DB::beginTransaction();

            if (!empty($request['id'])) {
                Aboutme::where('id', $request['id'])->update($validation);
                $message = ' Updated Existing Aboutme ';
            } else {
                Aboutme::create($insertData);
                $message = ' New Aboutme Details Added ';
            }
            tracking::create(['details' => $message,
                'name' => Auth::user()->name,
                'user_id' => Auth::user()->id,
                'uuid' => Auth::user()->uuid,
                'panal' => 'ADMIN',
            ]);
            toast($request['name'] . '' . $message, 'success', 'top-right')->persistent("Close");
            DB::commit();
            return redirect()->route('aboutme.index'); //->with('success', $request['name'] .''.$message);
        } catch (Exception $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }
}
