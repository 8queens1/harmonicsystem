<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Admin\Blog\Newsletteruser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Miscellaneous\helper;
use App\Models\Admin\Miscellaneous\tracking;
use Auth;
use DB;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use Newsletter;


class NewsletteruserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $newsletteruser = Newsletteruser::select(array('id', 'uniqid', 'mail', 'newsletter_status', 'updated_at', 'created_at'));
            return DataTables::of($newsletteruser)
                ->addColumn('action', function ($newsletteruser) {
                    if($newsletteruser->newsletter_status == 0){
                        return '<td class="text-right">
                         <a href="newsletteruserstatus/' . $newsletteruser->id . '/1" class="shadow rounded bg-green-500 hover:bg-green-600 p-2">Subscribe</a>
                        </td>';
                    }else{
                        return '<td class="text-right">
                         <a href="newsletteruserstatus/' . $newsletteruser->id . '/0" class="shadow rounded bg-yellow-500 text-white hover:bg-yellow-600 p-2">Unsubscribe</a>
                        </td>';
                    }
                })
                ->setRowClass(function ($newsletteruser) {
                    return $newsletteruser->newsletter_status == 1 ? 'text-success' : 'text-danger';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin/blog/newsletteruser/index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Model\/updates\newsletteruseregory
     * @return \Illuminate\Http\Response
     */
    public function newsletteruserstatus(newsletteruser $newsletteruser, $status)
    {

        if ($status == 1) {
            Newsletter::subscribeOrUpdate($newsletteruser->email);
        }else{
            Newsletter::unsubscribe($newsletteruser->email);
        }

        $newsletteruser->newsletter_status = $status;
        $newsletteruser->save();
        toast('Newsletters Status Updated successfully', 'success', 'top-right');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Model\/updates\newsletteruseregory
     * @return \Illuminate\Http\Response
     */
    public function destroy(newsletteruser $newsletteruser)
    {
        // $newsletteruser->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('newsletteruser.index');
    }

}
