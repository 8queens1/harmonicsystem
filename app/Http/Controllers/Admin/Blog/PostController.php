<?php

namespace App\Http\Controllers\Admin\Blog;

use DB;
use Auth;
use File;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Admin\Blog\Tag;
use App\Models\Admin\Blog\Post;
use Yajra\DataTables\DataTables;
use App\Models\Admin\Blog\Category;
use App\Http\Controllers\Controller;
use App\Models\Admin\Worldinfo\city;
use App\Models\Admin\Worldinfo\state;
use Intervention\Image\Facades\Image;
use App\Models\Admin\Blog\Subcategory;
use App\Models\Admin\Worldinfo\country;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin\Miscellaneous\helper;
use App\Models\Admin\Miscellaneous\tracking;
use App\Models\Admin\Officeutility\Eventcalendar;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('admin/blog/post/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(post $post)
    {
        $countries = DB::table("countries")->pluck("name", "id");
        $category = DB::table("categories")->pluck("name", "id");
        $post->publish = 1;
        $post->blogcomment = 1;
        return view('admin/blog/post/createOrUpdate', compact('post', 'countries', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // return $request->all();
        try {
            DB::beginTransaction();
            $validation = $this->validate($request, [
                'title' => 'required|max:200',
                'subtitle' => 'nullable|max:200',
                'postslug' => 'required',
                'interpretation' => 'required',
                'body' => 'required',
                'keyword' => 'required',

                'category_id' => 'required|not_in:0',
                'subcategory_id' => 'required|not_in:Select',

                'video_link' => 'nullable',
                'video_status' => 'nullable',

                'blogcomment' => 'nullable',

                'description' => 'required',
                'comments' => 'nullable',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $this->createorupdate($validation, $request);

            DB::commit();
            return redirect()->route('post.index');

        } catch (Exception $e) {
            DB::rollback();
            alert()->error('Exception', $e->getMessage())->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            alert()->error('Database Exception', $e->getMessage())->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            alert()->error('PDOException Exception', $e->getMessage())->persistent("Close");
            return redirect()->back();
        }
    }

    public function createorupdate($validation, $request)
    {

        // $validation['body']         = $this->domimageoptimization($validation['body']);
        $validation['publish'] = $request->has('publish');
        $validation['video_status'] = $request->has('video_status');
        $validation['blogcomment'] = $request->has('blogcomment');
        $validation['category_id'] = $request->category_id;
        $validation['subcategory_id'] = $request->subcategory_id;
        $validation['status'] = 1;
        $validation['active'] = 1;
        $validation['postslug'] = Str::slug($validation['postslug'], '-');
        $validation['user_id'] = Auth::user()->id;
        $validation['created_by'] = Auth::user()->name;

        if ($request->subcategory_id && $request->subcategory_id != 'Select' && $request->subcategory_id != 'null') {
            $validation['category'] = Category::find($request->category_id)->name;
            $validation['subcategory'] = Subcategory::find($request->subcategory_id)->name;
        }
        if ($request->country) {
            $validation['country_id'] = $request->country;
            $validation['country'] = country::find($request->country)->name;
        }
        if ($request->state && $request->state != 'Select' && $request->state != 'null') {
            $validation['state_id'] = $request->state;
            $validation['state'] = state::find($request->state)->name;
        }
        if ($request->city && $request->city != 'Select' && $request->city != 'null') {
            $validation['city_id'] = $request->city;
            $validation['city'] = city::find($request->city)->name;
        }

        if ($request->hasFile('image')) {
            if ($request['id']) {
                $image = post::where('id', $request['id'])->first()->image;
                if ($image) {
                    unlink(public_path() . '/images/blog/thumbnail/' . $image);
                    unlink(public_path() . '/images/blog/images/' . $image);
                }
            }

            $originalImage = $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path() . '/images/blog/thumbnail/';
            $originalPath = public_path() . '/images/blog/images/';
            // $imgname        = time() . $originalImage->getClientOriginalName();
            $imgname = time() . uniqid();
            $thumbnailImage->save($originalPath . $imgname, 70);
            $thumbnailImage->resize(250, 125);
            $thumbnailImage->save($thumbnailPath . $imgname);
            $validation['image'] = $imgname;

        }

        if (!empty($request['id'])) {

            $validation['body'] = $this->domimageoptimization($validation['body'], $request['uniqid']);
            post::where('id', $request['id'])->update($validation);
            $post = post::find($request['id']);
            // $post->categories()->sync($request->category_select);
            $post->tags()->sync($request->tag_select);

            toast('Post Updated successfully', 'success');
            $trackStatus = $request['uniqid'] . ' Updated Existing  Post';
        } else {

            $uniqueId = helper::getNextSequenceId(5, 'POST', 'App\Models\Admin\Blog\Post');
            $validation['sys_id'] = md5(uniqid(rand(), true));
            $validation['uniqid'] = $uniqueId['uniqid'];
            $validation['sequence_id'] = $uniqueId['sequence_id'];
            $validation['body'] = $this->domimageoptimization($validation['body'], $validation['uniqid']);
            $validation;
            $post = post::create($validation);
            Artisan::call('generate:sitemap');
            // $post->categories()->attach($request->category_select);
            $post->tags()->attach($request->tag_select);

            $date = Carbon::now();
            $uniqueId = helper::getNextSequenceId(5, 'APP', 'App\Models\Admin\Officeutility\Eventcalendar');
            Eventcalendar::create(['title' => $validation['uniqid'],
                'start' => $date->copy()->startOfDay(),
                'end' => $date->copy()->endOfDay(),
                'type' => 'AUTO',
                'color' => 'green',
                'active' => 1,
                'user_id' => Auth::user()->id,
                'created_by' => Auth::user()->name,
                'sys_id' => md5(uniqid(rand(), true)),
                'uniqid' => $uniqueId['uniqid'],
                'sequence_id' => $uniqueId['sequence_id'],
            ]);

            toast('New Post Created Successfully', 'success');
            $trackStatus = $validation['uniqid'] . ' Created New Post';
        }



        tracking::create(['details' => $trackStatus,
            'name' => Auth::user()->name,
            'user_id' => Auth::user()->id,
            'uuid' => Auth::user()->uuid,
            'panal' => 'ADMIN',
        ]);
    }

    public function domimageoptimization($body, $unique)
    {

        $path = public_path() . '/img/summernote/' . $unique;

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $dom = new \DomDocument();
        //@$dom->loadHtml($body, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        @$dom->loadHtml(mb_convert_encoding($body, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        // foreach <img> in the submited message
        foreach ($images as $img) {
            $src = $img->getAttribute('src');

            // if the img source is 'data-url'
            if (preg_match('/data:image/', $src)) {

                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];

                // Generating a random filename
                $filename = uniqid();
                $filepath = "/img/summernote/$unique/$filename.$mimetype";

                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                // resize if required
                /* ->resize(300, 200) */
                    ->encode($mimetype, 100) // encode file to the specified mimetype
                    ->save(public_path($filepath));

                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!--endforeach
        return $dom->saveHTML();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Models\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {
        return view('admin/blog/post/show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Models\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        // return $post;
        $countries = DB::table("countries")->pluck("name", "id");
        $category = DB::table("categories")->pluck("name", "id");
        return view('admin/blog/post/createOrUpdate', compact('post', 'countries', 'category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Models\/updates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        // $post->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('post.index');
    }

    public function ajaxtagcategory()
    {
        $category = category::all();
        $tag = tag::all();
        $categoryoption = '<option value="">SELECT CATEGORY</option>';
        foreach ($category as $row) {
            $categoryoption .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        $tagoption = '<option value="">SELECT TAG</option>';
        foreach ($tag as $row) {
            $tagoption .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        $output = array(
            'categoryoption' => $categoryoption,
            'tagoption' => $tagoption,
        );

        echo json_encode($output);
    }

    public function getSubcategroyList(Request $request)
    {
        $category = category::find($request->category_id)->subcategories->pluck('id');
        $subcategory = DB::table("subcategories")
            ->whereIn("id", $category)
            ->pluck("name", "id");
        return response()->json($subcategory);
    }

    public function getStateList(Request $request)
    {
        $states = DB::table("states")
            ->where("country_id", $request->country_id)
            ->pluck("name", "id");
        return response()->json($states);
    }

    public function getCityList(Request $request)
    {
        $cities = DB::table("cities")
            ->where("state_id", $request->state_id)
            ->pluck("name", "id");
        return response()->json($cities);
    }

    public function ajaxpost()
    {
        $post = post::select(array('id', 'title', 'uniqid', 'created_by', 'created_at', 'updated_at'));
        return DataTables::of($post)
            ->addColumn('action', function ($post) {
                return '<td class="text-right">
                   <a href="post/' . $post->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="post/' . $post->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
            })
            ->rawColumns(['action'])
            ->make(true);

    }

    public function fetch(Request $request)
    {
        if (strpos($request['uniqid'], 'POST') !== false) {
            $images = \File::allFiles(public_path('/img/summernote/' . $request['uniqid']));
            $output = '<div class="flex content-start flex-wrap h-50 sm:content-end md:content-center lg:content-between xl:content-around">';
            foreach ($images as $image) {
                $output .= ' <div class="sm:w-1/2 md:w-1/3 lg:w-1/2 xl:w-1/6 p-2">
              <div class="text-red text-center bg-white p-2">
              <img src="' . asset('/img/summernote/' . $request['uniqid'] . '/' . $image->getFilename()) . '" class="img-thumbnail" width="175" height="175" style="height:175px;" />
              <button type="button" style="color:red;" class="remove_image" id="' . $image->getFilename() . '">Remove</button>
              </div>
            </div>';
            }
            $output .= '</div>';
            echo $output;
        } else {
            echo '';
        }
    }

    public function deletegal(Request $request)
    {
        if ($request->get('name')) {
            \File::delete(public_path('/img/summernote/' . $request['uniqid'] . '/' . $request->get('name')));
        }
    }

}
