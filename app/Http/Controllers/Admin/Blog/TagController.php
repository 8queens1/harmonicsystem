<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Tag;
use App\Models\Admin\Miscellaneous\helper;
use App\Models\Admin\Miscellaneous\tracking;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $tag = tag::select(array('id', 'uniqid', 'name', 'created_by', 'created_at'));
            return DataTables::of($tag)
                ->addColumn('action', function ($tag) {
                    return '<td class="text-right">
                    <a href="tag/' . $tag->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="tag/' . $tag->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin/blog/tag/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(tag $tag)
    {
        return view('/admin/blog/tag/createorupdate', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $validation = $this->validate($request, [
                'name'        => 'required|max:35',
                'slug'        => 'required',
                'keyword'     => 'required',
                'description' => 'required',
                'comments'    => 'nullable',
            ]);

            $this->createorupdate($validation, $request);

            DB::commit();
            return redirect()->route('tag.index');

        } catch (Exception $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }

    public function createorupdate($validation, $request)
    {
        $validation['active']     = $request->has('active');
        $validation['status']     = 1;
        $validation['slug']       = Str::slug($validation['slug'], '-');
        $validation['user_id']    = Auth::user()->id;
        $validation['created_by'] = Auth::user()->name;

        if (!empty($request['id'])) {
            $validation['uniqid']      = $request['uniqid'];
            tag::where('id', $request['id'])->update($validation);
            toast('Tag Updated successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Updated Existing Tag';
        } else {
            $uniqueId                  = helper::getNextSequenceId(5, 'TAG', 'App\Models\Admin\Blog\Tag');
            $validation['sys_id']      = md5(uniqid(rand(), true));
            $validation['uniqid']      = $uniqueId['uniqid'];
            $validation['sequence_id'] = $uniqueId['sequence_id'];
            tag::create($validation);
            toast('New Tag Created Successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Created New Tag';
        }

        tracking::create(['details' => $trackStatus,
            'name'                      => Auth::user()->name,
            'user_id'                   => Auth::user()->id,
            'uuid'                      => Auth::user()->uuid,
            'panal'                     => 'ADMIN',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Modelupdates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function show(tag $tag)
    {
        return view('/admin/blog/tag/show', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Modelupdates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function edit(tag $tag)
    {
        return view('/admin/blog/tag/createorupdate', compact('tag'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Modelupdates\Categoryegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(tag $tag)
    {
        // $tag->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('tag.index');
    }

}
