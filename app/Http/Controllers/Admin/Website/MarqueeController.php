<?php

namespace App\Http\Controllers\Admin\Website;

use DB;
use Auth;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Models\Admin\Website\Marquee;
use App\Models\Admin\Miscellaneous\helper;
use App\Models\Admin\Miscellaneous\tracking;

class MarqueeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $marquee = Marquee::select(array('id', 'uniqid', 'marquee', 'active', 'created_by', 'created_at'));
            return DataTables::of($marquee)
                ->addColumn('active', function ($marquee) {
                    return ($marquee->active == 1) ? 'ACTIVE' : 'IN ACTIVE';
                })
                ->addColumn('action', function ($marquee) {
                    return '<td class="text-right">
                    <a href="marquee/' . $marquee->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="marquee/' . $marquee->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
                })
                ->rawColumns(['active', 'action'])
                ->make(true);
        }
        return view('admin/website/marquee/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(marquee $marquee)
    {
        return view('/admin/website/marquee/createorupdate', compact('marquee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $validation = $this->validate($request, [
                'marquee' => 'required||max:255',
                'remarks' => 'nullable|max:255',
            ]);

            if ($request->active) {
                Marquee::where('active', 1)->update(['active' => 0]);
            }

            $this->createorupdate($validation, $request);

            DB::commit();
            return redirect()->route('marquee.index');

        } catch (Exception $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }

    public function createorupdate($validation, $request)
    {

        $validation['user_id'] = Auth::user()->id;
        $validation['created_by'] = Auth::user()->name;
        $validation['active'] = $request->has('active');

        if (!empty($request['id'])) {
            $validation['updated_id'] = Auth::user()->id;
            $validation['updated_by'] = Auth::user()->name;
            Marquee::where('id', $request['id'])->update($validation);
            toast('Marquee Updated successfully', 'success', 'top-right');
            $trackStatus = $request['uniqid'] . ' Updated Existing Marquee';
        } else {
            $uniqueId = helper::getNextSequenceId(5, 'MARQUEE', 'App\Models\Admin\Website\Marquee');
            $validation['sys_id'] = md5(uniqid(rand(), true));
            $validation['uniqid'] = $uniqueId['uniqid'];
            $validation['sequence_id'] = $uniqueId['sequence_id'];
            Marquee::create($validation);
            toast('New Marquee Created Successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Created New Marquee';
        }

        tracking::create(['details' => $trackStatus,
            'name' => Auth::user()->name,
            'user_id' => Auth::user()->id,
            'uuid' => Auth::user()->uuid,
            'panal' => 'ADMIN',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Model\/updates\marqueeegory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $marquee = Marquee::findOrFail($id);
        return view('/admin/website/marquee/show', compact('marquee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Model\/updates\marqueeegory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marquee = Marquee::findOrFail($id);
        return view('/admin/website/marquee/createorupdate', compact('marquee'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Model\/updates\marqueeegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(marquee $marquee)
    {
        // $marquee->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('marquee.index');
    }

}
