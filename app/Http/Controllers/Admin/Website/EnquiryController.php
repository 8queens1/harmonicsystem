<?php

namespace App\Http\Controllers\Admin\Website;

use DB;
use Auth;
use Config;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Models\Admin\Website\Enquiry;
use App\Models\Admin\Miscellaneous\tracking;

class EnquiryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $enquiry = Enquiry::select(array('id', 'uniqid', 'fname', 'lname', 'email', 'enquirystatus', 'created_by', 'created_at'));
            return DataTables::of($enquiry)
                ->addColumn('enquirystatus', function ($enquiry) {
                    return Config::get('archive.enquirystatus')[$enquiry->enquirystatus];
                })
                ->addColumn('action', function ($enquiry) {
                    return '<td class="text-right">
                    <a href="enquiry/' . $enquiry->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="enquiry/' . $enquiry->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
                })
                ->rawColumns(['enquirystatus', 'action'])
                ->make(true);
        }
        return view('admin/website/enquiry/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Model\/updates\websitemarqueeegory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enquiry = Enquiry::findOrFail($id);
        return view('/admin/website/enquiry/show', compact('enquiry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Model\/updates\enquiryegory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enquiry = Enquiry::findOrFail($id);
        return view('/admin/website/enquiry/editform', compact('enquiry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $validation = $this->validate($request, [
                'enquirystatus' => 'required',
                'note'          => 'required|max:255',
            ]);

            $validation['updated_id'] = Auth::user()->id;
            $validation['updated_by'] = Auth::user()->name;
            Enquiry::where('id', $request['id'])->update($validation);
            toast('Enquiry Updated successfully', 'success', 'top-right');
            $trackStatus = $request['uniqid'] . ' Updated Existing Enquiry';

            tracking::create(['details' => $trackStatus,
                'name'                      => Auth::user()->name,
                'user_id'                   => Auth::user()->id,
                'uuid'                      => Auth::user()->uuid,
                'panal'                     => 'ADMIN',
            ]);

            DB::commit();
            return redirect()->route('enquiry.index');

        } catch (Exception $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        } catch (PDOException $e) {
            DB::rollback();
            toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
            return redirect()->back();
        }
    }
}
