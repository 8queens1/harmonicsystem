<?php

namespace App\Http\Controllers\Admin\Website;

use App\Http\Controllers\Controller;
use App\Models\Admin\Miscellaneous\tracking;
use App\Models\Admin\Website\Websitepages;
use Auth;
use DB;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use App\Models\Admin\Miscellaneous\helper;

class WebsitepagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables)
    {
        if (request()->ajax()) {
            $websitepages = Websitepages::select(array('id', 'uniqid', 'name', 'created_by', 'created_at'));
            return DataTables::of($websitepages)
                ->addColumn('action', function ($websitepages) {
                    return '<td class="text-right">
                    <a href="websitepages/' . $websitepages->id . '" class="shadow rounded bg-green-500 hover:bg-green-600 p-2"><i class="fa text-white fa-eye"></i></a>
                    <a href="websitepages/' . $websitepages->id . '/edit" class="shadow rounded bg-blue-500 hover:bg-blue-600 p-2"><i class="fa text-white fa-edit"></i></a>
                   </td>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin/website/websitepages/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(websitepages $websitepages)
    {
        return view('/admin/website/websitepages/createOrUpdate', compact('websitepages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  try {
            DB::beginTransaction();
            $validation = $this->validate($request, [
                'name' => 'required|max:100',
                'slug' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'page' => 'required',
                'comments' => 'nullable',
            ]);

            $this->createorupdate($validation, $request);

            DB::commit();
            return redirect()->route('websitepages.index');

        // } catch (Exception $e) {
        //     DB::rollback();
        //     toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
        //     return redirect()->back();
        // } catch (\Illuminate\Database\QueryException $e) {
        //     DB::rollback();
        //     toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
        //     return redirect()->back();
        // } catch (PDOException $e) {
        //     DB::rollback();
        //     toast('ERROR : ' . $e->getMessage(), 'error', 'top-right')->persistent("Close");
        //     return redirect()->back();
        // }
    }

    public function createorupdate($validation, $request)
    {
        $validation['active'] = $request->has('active');
        $validation['status'] = 1;
        $validation['slug'] = Str::slug($validation['slug'], '-');
        $validation['user_id'] = Auth::user()->id;
        $validation['created_by'] = Auth::user()->name;

        if (!empty($request['id'])) {
            $validation['uniqid'] = $request['uniqid'];
            $websitepages = Websitepages::where('id', $request['id'])->update($validation);
            $websitepages = Websitepages::find($request['id']);
            toast('Website Page Updated successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Updated Existing websitepages';
        } else {
            $uniqueId = helper::getNextSequenceId(5, 'WP', 'App\Models\Admin\Website\Websitepages');
            $validation['sys_id'] = md5(uniqid(rand(), true));
            $validation['uniqid'] = $uniqueId['uniqid'];
            $validation['sequence_id'] = $uniqueId['sequence_id'];
            $websitepages = Websitepages::create($validation);
            toast('New Website Page Created Successfully', 'success', 'top-right');
            $trackStatus = $validation['uniqid'] . ' Created New websitepages';
        }

        tracking::create(['details' => $trackStatus,
            'name' => Auth::user()->name,
            'user_id' => Auth::user()->id,
            'uuid' => Auth::user()->uuid,
            'panal' => 'ADMIN',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Model\/updates\websitepagesegory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $websitepages = Websitepages::find($id);
        return view('/admin/website/websitepages/show', compact('websitepages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Model\/updates\websitepagesegory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $websitepages = Websitepages::find($id);
        return view('/admin/website/websitepages/createOrUpdate', compact('websitepages'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Model\/updates\websitepagesegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(websitepages $websitepages)
    {
        // $websitepages->delete();
        // toast('Deleted Successfully', 'error', 'top-right');
        // return redirect()->route('websitepages.index');
    }

}
