<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Models\User;
use App\Models\Admin\Blog\Tag;
use App\Models\Admin\Blog\Post;
use Yajra\DataTables\DataTables;
use App\Models\Admin\Blog\Category;
use App\Http\Controllers\Controller;
use App\Models\Admin\Miscellaneous\tracking;
use App\Models\Admin\Miscellaneous\logininfo;
use App\Models\Admin\Officeutility\Eventcalendar;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $eventcalendar = Eventcalendar::where('active', 1)->get();
        $user = User::all();
        $tracking = tracking::orderBy('created_at', 'desc')->take(19)->get(); // ->where('user_id', '!=', 1)

        $totalpost = Post::get()->count();
        $totalpublishedpost = Post::where('publish', 1)->count();
        $totalcategory = Category::get()->count();
        $totaltag = Tag::get()->count();
        return view('admin.dashboard.dashboard', compact('eventcalendar', 'tracking', 'user', 'totalpost',
            'totalpublishedpost', 'totalcategory', 'totaltag'));
    }

    public function loginlogs(DataTables $datatables)
    {
        if (request()->ajax()) {
            $logininfo = logininfo::select(array('id', 'user_name', 'email', 'device', 'browser', 'platform', 'serverIp', 'clientIp', 'login_status', 'created_at'));
            return DataTables::of($logininfo)
                ->make(true);
        }
        return view('/admin/logs/loginlogs');
    }

    public function trackinglogs(DataTables $datatables)
    {
        if (request()->ajax()) {
            $tracking = tracking::select(array('id', 'name', 'details', 'created_at'));
            return DataTables::of($tracking)
                ->make(true);
        }
        return view('/admin/logs/trackinglogs');
    }

}
