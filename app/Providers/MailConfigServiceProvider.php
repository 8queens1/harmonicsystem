<?php

namespace App\Providers;

use Config;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if(App::make('configuration')){
            $config = array(
                'driver'     => App::make('configuration')->email_mail_driver,
                'host'       => App::make('configuration')->email_mail_host,
                'port'       => App::make('configuration')->email_mail_port,
                'username'   => App::make('configuration')->email_mail_username,
                'password'   => App::make('configuration')->email_mail_password,
                'encryption' => App::make('configuration')->email_mail_encryption,
                'from'       => array('address' => App::make('configuration')->email_from_mail, 'name' => App::make('configuration')->email_from_name),
                'sendmail'   => '/usr/sbin/sendmail -bs',
                'pretend'    => false,
            );
            Config::set('mail', $config);
        }
    }
}

