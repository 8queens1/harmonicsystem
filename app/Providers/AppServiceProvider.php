<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\Models\Admin\Settings\configuration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('configuration', function () {
            return configuration::where('uuid', '88fc5a4d-1221-478b-aba2-blogwebtool')->first();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(App::make('configuration')){
            date_default_timezone_set(App::make('configuration')->timezone);
            config(['app.timezone' => App::make('configuration')->timezone]);
            config(['app.date_format' => App::make('configuration')->dateformate]);
            config(['app.date_format_javascript' => App::make('configuration')->dateformate]);
            config(['app.url' => App::make('configuration')->website]);
            
            config(['newsletter.apiKey' => App::make('configuration')->mailchimpapikey]);
            config(['newsletter.lists.subscribers.id' => App::make('configuration')->mailchimplistid]);

            config(['captcha.sitekey' => App::make('configuration')->recaptchasitekey]);
            config(['captcha.secret' => App::make('configuration')->recaptchasecretkey]);

            config(['scout.algolia.id' => App::make('configuration')->algoliaapp]);
            config(['scout.algolia.secret' => App::make('configuration')->algoliasecret]);
            
        }
    }
}
