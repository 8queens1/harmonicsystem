<?php

namespace App\Mail\website;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class admincontactusmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.admincontactusmail')
            ->subject('New Enquiry From ' . $this->data['fname'])
            ->with([
                'fname' => $this->data['fname'],
                'lname' => $this->data['lname'],
                'email' => $this->data['email'],
                'message' => $this->data['message'],
            ]);

    }
}
