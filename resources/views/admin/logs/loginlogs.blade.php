@extends('components.admin.layout.app')
@section('headSection')
@endsection

@section('main-content')
<main class="w-full flex-grow p-3">
    <x-admin.layout.indexnav name=enquiry title="LOGIN LOGS" button='disable' gate='true' />
       <!--Card-->
       <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
          <table id="ajaxloginlogs" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
             <thead class="bg-blue-600 text-white">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>DEVICE</th>
                    <th>BROWSER</th>
                    <th>PLATEFORM</th>
                    <th>SERVER IP</th>
                    <th>CLIENT IP</th>
                    <th>LOGIN STATUS</th>
                    <th>CREATED AT</th>
                </tr>
             </thead>
          </table>
       </div>
       <!--/Card-->
 </main>
@endsection 

@section('footerSection')
<script type="text/javascript">
    $('#ajaxloginlogs').DataTable({
     processing: true,
     "order": [[ 0, "desc" ]],
     serverSide: true,
     "ajax": {
         "url": '{!! route('loginlogs') !!}',
         'headers': {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
         "type": "GET"
     },
     columns: [
        { data: 'id', name: 'id' },
         { data: 'user_name', name: 'user_name' },
         { data: 'email', name: 'email' },
         { data: 'device', name: 'device' },
         { data: 'browser', name: 'browser' },
         { data: 'platform', name: 'platform' },
         { data: 'serverIp', name: 'serverIp' },
         { data: 'clientIp', name: 'clientIp' },
         { data: 'login_status', name: 'login_status' },
         { data: 'created_at', name: 'created_at' },
     ]
    });
 </script>
@endsection