@extends('components.admin.layout.app')
@section('headSection')
@endsection

@section('main-content')
<main class="w-full flex-grow p-3">
    <x-admin.layout.indexnav name=enquiry title="TRACKING LOGS" button='disable' gate='true' />
       <!--Card-->
       <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
          <table id="ajaxtrackinglogs" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
             <thead class="bg-blue-600 text-white">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>DETAILS</th>
                    <th>CREATED AT</th>
                </tr>
             </thead>
          </table>
       </div>
       <!--/Card-->
 </main>
@endsection 

@section('footerSection')
<script type="text/javascript">
    $('#ajaxtrackinglogs').DataTable({
     processing: true,
     "order": [[ 0, "desc" ]],
     serverSide: true,
     "ajax": {
         "url": '{!! route('trackinglogs') !!}',
         'headers': {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
         "type": "GET"
     },
     columns: [
         { data: 'id', name: 'id' },
         { data: 'name', name: 'name' },
         { data: 'details', name: 'details' },
         { data: 'created_at', name: 'created_at' },
     ]
    });
 </script>
@endsection