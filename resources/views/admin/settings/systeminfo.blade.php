@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow p-3">
   <x-admin.layout.createoreditnav  name=systemifno title="SYSTEM INFO" :obj="auth()->user()" backbutton="settings" />
   <div class="p-2 mt-4 lg:mt-0 rounded shadow bg-white">
      <iframe class="w-full h-screen" src="{{route('decompose')}}">Your browser isn't compatible</iframe>
   </div>
   </div>
</main>
@endsection 
@section('footerSection')
@endsection