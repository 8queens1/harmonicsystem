
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "company_name", 'name' => "COMPANY NAME", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('company_name',$configuration->company_name ,array('id'=>'company_name', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "company_name"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "company_full_name", 'name' => "COMPANY FULL NAME", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('company_full_name',$configuration->company_full_name ,array('id'=>'company_full_name', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "company_full_name"])
    </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "website", 'name' => "WEBSITE", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('website',$configuration->website ,array('id'=>'website', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "website"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "uplode_logo_image", 'name' => "UPLOAD LOGO", 'required' => false])
    </div>
    <div class="md:w-4/12">
      <input type="file" name="uplode_logo_image"  accept="image/*" id="uplode_logo_image" class="form-input rounded block w-full p-1 focus:bg-white"> 
       @include('helper.formerror', ['error' => "uplode_logo_image"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "favicon_image", 'name' => "FAVICON IMAGE", 'required' => false])
    </div>
    <div class="md:w-4/12">
      <input type="file" name="favicon_image"  accept="image/*" id="favicon_image" class="form-input rounded block w-full p-1 focus:bg-white"> 
      @include('helper.formerror', ['error' => "favicon_image"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "theme_color", 'name' => "THEME COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::select('theme_color', $themecolor, $configuration->theme_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "theme_color"])
    </div>
</div>



<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "text_color", 'name' => "TEXT COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('text_color', $textcolor, $configuration->text_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "text_color"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "background_color", 'name' => "BACKGROUND COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('background_color', $themecolor, $configuration->background_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "background_color"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "headerbg_color", 'name' => "HEADER BG COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('headerbg_color', $themecolor, $configuration->headerbg_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "headerbg_color"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "headertext_color", 'name' => "HEADER TEXT COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('headertext_color', $textcolor, $configuration->headertext_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "headertext_color"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "footerbg_color", 'name' => "FOOTER BG COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('footerbg_color', $themecolor, $configuration->footerbg_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "footerbg_color"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "footertext_color", 'name' => "FOOTER TEXT COLOR", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('footertext_color', $textcolor, $configuration->footertext_color, array('class' => 'form-input rounded block w-full p-1 focus:bg-white', 'readonly' => 'true')) }}
       @include('helper.formerror', ['error' => "footertext_color"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "timezone", 'name' => "TIME ZONE", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('timezone',$configuration->timezone ,array('id'=>'timezone', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "timezone"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "dateformate", 'name' => "DATE FORMATE", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('dateformate',$configuration->dateformate ,array('id'=>'dateformate', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "dateformate"])
    </div>
</div>

<div class="md:flex mb-3">

   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "blogtemplates", 'name' => "BLOG TEMPLATE", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('blogtemplates', ['TEMPLATE ONE', 'TEMPLATE TWO'], $configuration->blogtemplates, ['class' => 'form-select rounded block w-full focus:bg-white  p-1']) }}
       @include('helper.formerror', ['error' => "blogtemplates"])
    </div>

   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "copyrights", 'name' => "COPY RIGHTS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('copyrights',$configuration->copyrights ,array('id'=>'copyrights', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "copyrights"])
    </div>
</div>


<div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "remarks", 'name' => "REMARKS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::textarea('remarks',$configuration->remarks ,array('id'=>'', 'class'=>'form-textarea rounded block w-full focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "remarks"])
    </div>
</div>



<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
    <i class="fas fa-list mr-3"></i>  MAIL CREDENTIALS
</p>





<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_from_name", 'name' => "FROM NAME", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_from_name',$configuration->email_from_name ,array('id'=>'email_from_name', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_from_name"])
    </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_from_mail", 'name' => "FROM EMAIL", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_from_mail',$configuration->email_from_mail ,array('id'=>'email_from_mail', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_from_mail"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_mail_driver", 'name' => "MAIL DRIVER", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_mail_driver',$configuration->email_mail_driver ,array('id'=>'email_mail_driver', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_mail_driver"])
    </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_mail_host", 'name' => "MAIL HOST", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_mail_host',$configuration->email_mail_host ,array('id'=>'email_mail_host', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_mail_host"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_mail_port", 'name' => "MAIL PORT", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_mail_port',$configuration->email_mail_port ,array('id'=>'email_mail_port', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_mail_port"])
    </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_mail_username", 'name' => "MAIL USERNAME", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_mail_username',$configuration->email_mail_username ,array('id'=>'email_mail_username', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_mail_username"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_mail_password", 'name' => "MAIL PASSWORD", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_mail_password',$configuration->email_mail_password ,array('id'=>'email_mail_password', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_mail_password"])
    </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email_mail_encryption", 'name' => "MAIL ENCRYPTION", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email_mail_encryption',$configuration->email_mail_encryption ,array('id'=>'email_mail_encryption', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email_mail_encryption"])
    </div>
</div>




<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
   <i class="fas fa-list mr-3"></i>  SOCIAL MEDIA LINK
</p>

<div class="md:flex mb-3">
  <div class="md:w-2/12">
     @include('helper.formlabel', ['for' => "facebook", 'name' => "FACEBOOK URL", 'required' => false])
   </div>
   <div class="md:w-4/12">
      {{ Form::text('facebook',$configuration->facebook ,array('id'=>'facebook', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "facebook"])
   </div>
  <div class="md:w-2/12">
     @include('helper.formlabel', ['for' => "twitter", 'name' => "TWITTER URL", 'required' => false])
   </div>
   <div class="md:w-4/12">
      {{ Form::text('twitter',$configuration->twitter ,array('id'=>'twitter', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "twitter"])
   </div>
</div>




<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "instagram", 'name' => "INSTAGRAM URL", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('instagram',$configuration->instagram ,array('id'=>'instagram', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "instagram"])
    </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "linkedin", 'name' => "LINKED IN URL", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('linkedin',$configuration->linkedin ,array('id'=>'linkedin', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "linkedin"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "youtube", 'name' => "YOUTUBE URL", 'required' => false])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('youtube',$configuration->youtube ,array('id'=>'youtube', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "youtube"])
    </div>
</div>


<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
  <i class="fas fa-list mr-3"></i> API KEY SETTINGS
</p>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "mailchimpflag", 'name' => "MAILCHIMP STATUS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('mailchimpflag', ['DISABLE', 'ENABLE'], $configuration->mailchimpflag, ['class' => 'form-select rounded block w-full focus:bg-white  p-1']) }}
       @include('helper.formerror', ['error' => "mailchimpflag"])
    </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "mailchimpapikey", 'name' => "MAILCHIMP API KEY", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('mailchimpapikey',$configuration->mailchimpapikey ,array('id'=>'mailchimpapikey', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "mailchimpapikey"])
    </div>
</div>

<div class="md:flex mb-3">
<div class="md:w-2/12">
   @include('helper.formlabel', ['for' => "mailchimplistid", 'name' => "MAILCHIMP LIST ID", 'required' => false])
 </div>
 <div class="md:w-4/12">
   {{ Form::text('mailchimplistid',$configuration->mailchimplistid ,array('id'=>'mailchimplistid', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
    @include('helper.formerror', ['error' => "mailchimplistid"])
 </div>
</div>

<hr class="my-4">

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "disqusflag", 'name' => "BLOG COMMENT STATUS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('disqusflag', ['DISABLE', 'ENABLE'], $configuration->disqusflag, ['class' => 'form-select rounded block w-full focus:bg-white  p-1']) }}
       @include('helper.formerror', ['error' => "disqusflag"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "disquscode", 'name' => "DISQUS COMMENT CODE", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::textarea('disquscode',$configuration->disquscode ,array('id'=>'', 'class'=>'form-textarea rounded block w-full focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "disquscode"])
    </div>
</div>

<hr class="my-4">

<div class="md:flex mb-3">
   {{-- <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "googleanalyticsapi", 'name' => "GOOGLE ANALYTICS API", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('googleanalyticsapi',$configuration->googleanalyticsapi ,array('id'=>'googleanalyticsapi', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "googleanalyticsapi"])
    </div> --}}
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "googleanalyticscode", 'name' => "GOOGLE ANALYTICS CODE", 'required' => false])
    </div>
    <div class="md:w-10/12">
      {{ Form::textarea('googleanalyticscode',$configuration->googleanalyticscode ,array('id'=>'', 'class'=>'form-textarea rounded block w-full p-1 focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "googleanalyticscode"])
    </div>
</div>


<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
   <i class="fas fa-list mr-3"></i> BLOG SEARCH
 </p>
 
 

 
 <div class="md:flex mb-3">
    <div class="md:w-2/12">
       @include('helper.formlabel', ['for' => "searchstatus", 'name' => "SEARCH STATUS", 'required' => false])
     </div>
     <div class="md:w-4/12">
       {{ Form::select('searchstatus', ['DISABLE', 'ENABLE', 'AGOLIA SEARCH'], $configuration->searchstatus, ['class' => 'form-select rounded block w-full focus:bg-white  p-1']) }}
        @include('helper.formerror', ['error' => "searchstatus"])
     </div>

     <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "algoliaapp", 'name' => "ALGOLIA APP ID", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('algoliaapp',$configuration->algoliaapp ,array('id'=>'algoliaapp', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "algoliaapp"])
    </div>
 </div>

 <div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "algoliasecret", 'name' => "ALGOLIA SECRET", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('algoliasecret',$configuration->algoliasecret ,array('id'=>'algoliasecret', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "algoliasecret"])
    </div>
</div>


<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
   <i class="fas fa-list mr-3"></i> GOOGLE CAPTCHA
 </p>
 
 

 
 <div class="md:flex mb-3">
    <div class="md:w-2/12">
       @include('helper.formlabel', ['for' => "recaptchasecretstatus", 'name' => "CAPTCHA STATUS", 'required' => false])
     </div>
     <div class="md:w-4/12">
       {{ Form::select('recaptchasecretstatus', ['DISABLE', 'ENABLE'], $configuration->recaptchasecretstatus, ['class' => 'form-select rounded block w-full focus:bg-white  p-1']) }}
        @include('helper.formerror', ['error' => "recaptchasecretstatus"])
     </div>

     <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "recaptchasitekey", 'name' => "CAPTCHA SITE KEY", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('recaptchasitekey',$configuration->recaptchasitekey ,array('id'=>'recaptchasitekey', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "recaptchasitekey"])
    </div>
 </div>

 <div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "recaptchasecretkey", 'name' => "CAPTCHA SECRET KEY", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('recaptchasecretkey',$configuration->recaptchasecretkey ,array('id'=>'recaptchasecretkey', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "recaptchasecretkey"])
    </div>
</div>

<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
   <i class="fas fa-list mr-3"></i> GOOGLE ADS
 </p>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "googleadsverticalcode", 'name' => "VERTICAL SCRIPT CODE", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::textarea('googleadsverticalcode',$configuration->googleadsverticalcode ,array('id'=>'', 'class'=>'form-textarea rounded block w-full p-1 focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "googleadsverticalcode"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "googleadshorizontalcode", 'name' => "HORIZONTAL SCRIPT CODE", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::textarea('googleadshorizontalcode',$configuration->googleadshorizontalcode ,array('id'=>'', 'class'=>'form-textarea rounded block w-full p-1 focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "googleadshorizontalcode"])
    </div>
</div>



<p class="text-xl p-2 flex items-center bg-blue-200 mb-6">
   <i class="fas fa-list mr-3"></i> SOCAIAL MEDIAL SHARE LINK SOURCE CODE
 </p>

 <div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "socialmediaicon", 'name' => "VERTICAL SCRIPT CODE", 'required' => false])
    </div>
    <div class="md:w-10/12">
      {{ Form::textarea('socialmediaicon',$configuration->socialmediaicon ,array('id'=>'', 'class'=>'form-textarea rounded  P-1 block w-full focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "socialmediaicon"])
    </div>
</div>