@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow p-3">
   <x-admin.layout.indexnav name=setting title="SETTINGS" button='disable' gate='true' />

   <div class="p-2 mt-4 lg:mt-0 rounded shadow bg-white">
      <div class="grid gap-6 md:grid-cols-2 xl:grid-cols-3 m-5">
         <!-- Card -->
         <div class="flex items-center bg-blue-100 px-5 py-2 rounded-lg shadow-xs dark:bg-gray-800">
            <div class="pb-4">
               <p class="mb-2 text-2xl font-medium  text-gray-800 dark:text-gray-400">
                  USER
               </p>
               <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                <a href="{{ URL('admin/addemployee') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold px-3 py-2 m-1 rounded">
                    ADD USER
                  </a>
                  <a href="{{ URL('admin/changepasswordform') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold px-3  py-2 m-1 rounded">
                    CHANGE PASSWORD
                  </a>
               </p>
            </div>
         </div>
         <!-- Card -->
         <div class="flex items-center bg-blue-100 px-5 py-3 rounded-lg shadow-xs dark:bg-gray-800">
            <div class="pb-4">
               <p class="mb-2 text-2xl font-medium  text-gray-800 dark:text-gray-400">
                  CONFIGURATION
               </p>
               <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                <a href="{{ URL('admin/configuration') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold px-3 py-2 m-1 rounded">
                    CONFIGURATION
                  </a>
                  <a href="{{ URL('admin/cacheclear') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold px-3 py-2 m-1 rounded">
                     CLEAR CACHE
                  </a>
               </p>
            </div>
         </div>

         <!-- Card -->
         <div class="flex items-center bg-blue-100 px-5 py-3 rounded-lg shadow-xs dark:bg-gray-800">
            <div class="pb-4">
               <p class="mb-2 text-2xl font-medium  text-gray-800 dark:text-gray-400">
                  SYSTEM IFORMATION
               </p>
               <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                  <a href="{{ URL('admin/systeminfo') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold px-3 py-2 m-1 rounded">
                     SYSTEM INFO
                  </a>
                  <a href="{{ URL('admin/backuprun') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold px-3 py-2 m-1 rounded">
                     DATA BACKUP
                  </a>
               </p>
            </div>
         </div>

      </div>
   </div>
   
</main>
@endsection 
@section('footerSection')
@endsection