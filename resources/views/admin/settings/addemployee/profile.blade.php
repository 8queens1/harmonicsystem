@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='addemployee' title="USER - {{ $addemployee->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $addemployee->uniqid }}" />
         <x-admin.layout.showlabeldetails title='NAME' value="{{ $addemployee->name }}" />
      </div>
      <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='EMAIL' value="{{ $addemployee->email }}" />
        <x-admin.layout.showlabeldetails title='ADDRESS' value="{{ $addemployee->address }}" />
     </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='STATUS' value="{{ isset($addemployee) && ($addemployee->status == 0)  ? 'Active' : 'In Active' }}" />
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $addemployee->remarks }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $addemployee->created_by }}" />
        <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $addemployee->created_at }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $addemployee->updated_at }}" />
      </div>
   </div>
</main>
@endsection
@section('footerSection')
@endsection