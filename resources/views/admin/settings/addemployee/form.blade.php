<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "name", 'name' => "NAME", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('name',$addemployee->name ,array('id'=>'name', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "name"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "email", 'name' => "EMAIL", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('email',$addemployee->email ,array('id'=>'email', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "email"])
    </div>
</div>


<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "password", 'name' => "LOGIN PASSWORD", 'required' => true])
    </div>
    <div class="md:w-4/12">
       {{ Form::text('password',$addemployee->password ,array('id'=>'password', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "password"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "password_confirmation", 'name' => "CONFIRM PASSWORD", 'required' => true])
    </div>
    <div class="md:w-4/12">
      {{ Form::text('password_confirmation',$addemployee->password_confirmation ,array('id'=>'password_confirmation', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "password_confirmation"])
    </div>
</div>



<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "avatar", 'name' => "PROFILE AVATAR", 'required' => false])
    </div>
    <div class="md:w-4/12">
      <input type="file" name="avatar"  accept="image/*" id="avatar" class="form-input rounded block w-full p-1 focus:bg-white"> 
      @include('helper.formerror', ['error' => "avatar"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "status", 'name' => "USER STATUS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::select('status', ['ACTIVE', 'INACTIVE'], $addemployee->status, ['class' => 'form-select block w-full focus:bg-white p-1']) }}
       @include('helper.formerror', ['error' => "status"])
    </div>
</div>






<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "address", 'name' => "ADDRESS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::textarea('address',$addemployee->address ,array('id'=>'', 'class'=>'form-textarea block w-full focus:bg-white p-1', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "address"])
    </div>
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "remarks", 'name' => "REMARKS", 'required' => false])
    </div>
    <div class="md:w-4/12">
      {{ Form::textarea('remarks',$addemployee->remarks ,array('id'=>'', 'class'=>'form-textarea block w-full focus:bg-white p-1', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "remarks"])
    </div>
</div>

