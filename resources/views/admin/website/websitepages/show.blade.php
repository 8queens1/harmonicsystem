@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='websitepages' title="WEBSITE PAGES - {{ $websitepages->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $websitepages->uniqid }}" />
         <x-admin.layout.showlabeldetails title='NAME' value="{{ $websitepages->name }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='STATUS' value="{{ isset($websitepages) && $websitepages->active ? 'Active' : 'In Active' }}" />
         <x-admin.layout.showlabeldetails title='SLUG' value="{{ $websitepages->slug }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='SEO KEYWORD' value="{{ $websitepages->keyword }}" />
         <x-admin.layout.showlabeldetails title='SEO DESCRIPTION' value="{{ $websitepages->description }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='PAGE' value="{!! $websitepages->page !!}" />
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $websitepages->comments }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $websitepages->created_by }}" />
         <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $websitepages->created_at }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $websitepages->updated_at }}" />
      </div>
   </div>
</main>
@endsection
@section('footerSection')
@endsection