@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='marquee' title="MARQUEE - {{ $marquee->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $marquee->uniqid }}" />
         <x-admin.layout.showlabeldetails title='MARQUEE NAME' value="{{ $marquee->marquee }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='STATUS' value="{{ isset($marquee) && $marquee->active ? 'Active' : 'In Active' }}" />
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $marquee->remarks }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $marquee->created_by }}" />
        <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $marquee->created_at }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $marquee->updated_at }}" />
      </div>
   </div>
</main>
@endsection
@section('footerSection')
@endsection