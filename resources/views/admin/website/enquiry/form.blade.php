<div class="md:flex mb-6">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "enquirystatus", 'name' => "ENQUIRY STATUS", 'required' => true]) 
    </div>
    <div class="md:w-2/12 p-1">
       <select name="enquirystatus" class="form-select block w-full focus:bg-white p-1" id="enquirystatus" readonly>
            @foreach(config('archive.enquirystatus') as $key => $value)
                <option  value="{{ $key }}" {{ ($enquiry->enquirystatus == $key) ? 'selected' : '' }} >
                {{ $value }}
                </option>
            @endforeach
       </select>
       @include('helper.formerror', ['error' => "enquirystatus"])
    </div>

    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "note", 'name' => "REMARKS", 'required' => true]) 
    </div>
    <div class="md:w-10/12">
       {{ Form::textarea('note',$enquiry->note ,array('id'=>'note','class'=>'p-1 form-textarea block w-full focus:bg-white', 'rows'=>'2')) }}
        @include('helper.formerror', ['error' => "note"])
    </div>
 </div>