@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='enquiry' title="CONTACT US - {{ $enquiry->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $enquiry->uniqid }}" />
         <x-admin.layout.showlabeldetails title='FIRST NAME' value="{{ $enquiry->fname }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='LAST NAME' value="{{ $enquiry->lname }}" />
         <x-admin.layout.showlabeldetails title='EMAIL' value="{{ $enquiry->email }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='MESSAGE' value="{{ $enquiry->message }}" />
         <x-admin.layout.showlabeldetails title='ADMIN STATUS' value="{{ config('archive.enquirystatus')[$enquiry->enquirystatus] }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ADMIN NOTE' value="{{ $enquiry->note }}" />
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $enquiry->created_by }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $enquiry->created_at }}" />
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $enquiry->updated_at }}" />
      </div>
   </div>
</main>
@endsection
@section('footerSection')
@endsection