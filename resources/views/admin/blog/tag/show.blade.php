@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='tag' title="TAG - {{ $tag->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $tag->uniqid }}" />
         <x-admin.layout.showlabeldetails title='TAG NAME' value="{{ $tag->name }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='STATUS' value="{{ isset($tag) && $tag->active ? 'Active' : 'In Active' }}" />
         <x-admin.layout.showlabeldetails title='SLUG' value="{{ $tag->slug }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='SEO KEYWORD' value="{{ $tag->keyword }}" />
         <x-admin.layout.showlabeldetails title='SEO DESCRIPTION' value="{{ $tag->description }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $tag->comments }}" />
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $tag->created_by }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $tag->created_at }}" />
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $tag->updated_at }}" />
      </div>
   </div>
</main>
@endsection
@section('footerSection')
@endsection