@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')

<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.createoreditnav  name=post title="POST" :obj="$post" backbutton="enable" />
    <!--Card-->
    <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
       {!! Form::model($post, ['route' => ['post.store', $post->id],  'id' => '', 'class' => ' form_prevent_multiple_submits ', 'novalidate' => 'novalidate', 'files' => 'true','enctype'=>'multipart/form-data']) !!}
       {{ Form::hidden('id', $post->id, array('id' => 'invisible_id')) }}
       {{ Form::hidden('uniqid', $post->uniqid, array('id' => 'invisible_id')) }}

        @include('admin.blog.post.form') 

        <div class="md:flex md:items-center justify-center">
         @if ($post->id)
         {!! Form::button( '<i class="fa fa-spinner m-1 fa-spin"></i> UPDATE', ['type' => 'submit', 'class' => 'shadow bg-green-500 hover:bg-green-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 m-1 flex rounded button_prevent_multiple_submits'] ) !!}
         @else
         {!! Form::button( '<i class="fa fa-spinner m-1 fa-spin"></i> CREATE', ['type' => 'submit', 'class' => 'shadow bg-green-500 hover:bg-green-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 m-1 flex rounded button_prevent_multiple_submits'] ) !!}
         @endif
         <a href="" class="shadow bg-gray-500 hover:bg-gray-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">CANCEL</a>
       </div>

       {!! Form::close() !!}
    </div>
 </main>

 <div class="m-6 flex flex-wrap bg-white shadow">
   <div class="w-full pr-0 lg:pr-2">
       <p class="text-xl p-3 flex items-center">
           <i class="fas fa-plus mr-3"></i> List Of Image Uploaded
       </p>
       <div class="p-6">
         <div class="card-body" id="uploaded_image">
       </div>
   </div>
  </div>
 </div>



@endsection
@section('footerSection')
<style type="text/css">
    @import  url(https://fonts.googleapis.com/css?family=Open+Sans:400italic);
  article{
    font-size: 1.4em;
    font-family:Open Sans;
    font-style:italic;
    color: #555555;
    padding:1.2em 30px 1.2em 75px;
    border-left:8px solid {{ App::make('configuration')->theme_color }};
    line-height:1.6;
    position: relative;
    background:#EDEDED;
  }
  
  article::before{
    font-family:Arial;
    content: "\201C";
    color:{{ App::make('configuration')->theme_color }};
    font-size:4em;
    position: absolute;
    left: 10px;
    top:-10px;
  }
  
  article::after{
    content: '';
  }
  
  article span{
    display:block;
    color:#333333;
    font-style: normal;
    font-weight: bold;
    margin-top:1em;
  }
  </style>  
<style>
    .note-editable{
        background: white;
    }


</style>

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
 <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>



 
 <script type="text/javascript">
   $('.summernote').summernote({
    placeholder: 'Short Description About Blog',
    tabsize: 2,
    height: 100,
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
    ]

});

var HelloButton = function(context) {
    var ui = $.summernote.ui;
    // create button
    var button = ui.button({
        contents: '<i class="fa fa-quote-left"/> Quotes',
        tooltip: 'hello',
        click: function() {
            // invoke insertText method with 'hello' on editor module.
            context.invoke('editor.pasteHTML', '<div class="container"><blockquote  class="blockquote"><article class="font-weight-bold">Creativity is the key to success in the future, and primary education is where teachers can bring creativity in children at that level<footer class="blockquote-footer"><cite title="Source Title">A.P.J.Abdul Kalam</cite></footer></article></blockquote></div>');
        }
    });

    return button.render(); // return button as jquery object 
}

$('.summernote_two').summernote({
    placeholder: 'Post Your Blog Here',
    tabsize: 2,
    height: 400,
    toolbar: [
        ['mybutton', ['quotes']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['style', ['style']],
        ['fontname', ['fontname']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
        ['misc', ['undo', 'redo', 'print']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
    buttons: {
        quotes: HelloButton,
    },
    popover: {
        image: [
            ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
            ['float', ['floatLeft', 'floatRight', 'floatNone']],
            ['remove', ['removeMedia']],
        ],
        link: [
            ['link', ['linkDialogShow', 'unlink']]
        ],
        table: [
            ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
            ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
        ],
        air: [
            ['color', ['color']],
            ['font', ['bold', 'underline', 'clear']],
            ['para', ['ul', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture']]
        ],
    }
});

$(document).ready(function() {
    $('.js-example-basic-multiple_one').select2();
    $('.js-example-basic-multiple_two').select2();

    ajaxtagcategory();

    function ajaxtagcategory() {
        $.ajax({
            url: "{{route('ajaxtagcategory')}}",
            mehtod: "get",
            dataType: 'json',
            success: function(data) {
             $('#categoryoption').html(data.categoryoption);
             $('#tagoption').html(data.tagoption);
             $('#categoryoption').val({!! $post->categorySelect !!});
             $('#categoryoption').trigger('change');
             $('#tagoption').val({!! $post->tagSelect !!});
             $('#tagoption').trigger('change');
            }
        })
    }
});
 </script>
 <script type="text/javascript">

 $('#category').change(function(){
     var categoryID = $(this).val();
     if(categoryID){
         $.ajax({
            type:"GET",
            url:"{{url('admin/getSubcategroyList')}}?category_id="+categoryID,
            success:function(res){
             if(res){
                 $("#subcategory_id").empty();
                 $("#subcategory_id").append('<option>Select</option>');
                 $.each(res,function(key,value){
                     $("#subcategory_id").append('<option value="'+key+'">'+value+'</option>');
                 });
 
             }else{
                $("#subcategory_id").empty();
             }
            }
         });
     }else{
         $("#subcategory_id").empty();
     }
    });
 
    subcategorykey = {!! json_encode($post->subcategory_id) !!};
    subcategoryname = {!! json_encode($post->subcategory) !!};
    $("#subcategory_id").append('<option value="'+subcategorykey+'">'+subcategoryname+'</option>');
 
 
   statekey = {!! json_encode($post->state_id) !!};
   statename = {!! json_encode($post->state) !!};
   $("#state").append('<option value="'+statekey+'">'+statename+'</option>');
   citykey = {!! json_encode($post->city_id) !!};
   cityname = {!! json_encode($post->city) !!};
   $("#city").append('<option value="'+citykey+'">'+cityname+'</option>');
 
     $('#country').change(function(){
     var countryID = $(this).val();
     if(countryID){
         $.ajax({
            type:"GET",
            url:"{{url('admin/getStateList')}}?country_id="+countryID,
            success:function(res){
             if(res){
                 $("#state").empty();
                 $("#state").append('<option>Select</option>');
                 $.each(res,function(key,value){
                     $("#state").append('<option value="'+key+'">'+value+'</option>');
                 });
 
             }else{
                $("#state").empty();
             }
            }
         });
     }else{
         $("#state").empty();
         $("#city").empty();
     }
    });
     $('#state').on('change',function(){
     var stateID = $(this).val();
     if(stateID){
         $.ajax({
            type:"GET",
            url:"{{url('admin/getCityList')}}?state_id="+stateID,
            success:function(res){
             if(res){
                 $("#city").empty();
                 $("#city").append('<option>Select</option>');
                 $.each(res,function(key,value){
                     $("#city").append('<option value="'+key+'">'+value+'</option>');
                 });
 
             }else{
                $("#city").empty();
             }
            }
         });
     }else{
         $("#city").empty();
     }
 
    });
 </script>
 
 <script>
 load_images();
 
 function load_images()
 {
   $.ajax({
     url:"{{ route('postimg.fetch', ['uniqid' => $post->uniqid]) }}",
     success:function(data)
     {
       $('#uploaded_image').html(data);
     }
   })
 }
 
 $(document).on('click', '.remove_image', function(){
   var name = $(this).attr('id');
   var uniqid = {!! json_encode($post->uniqid) !!};
   $.ajax({
     url:"{{ route('postimg.deletegal') }}",
     data:{ "_token": "{{ csrf_token() }}", "name" : name, "uniqid":uniqid},
     method:"POST",
     success:function(data){
       load_images();
     }
   })
 });
 
 </script>
@endsection