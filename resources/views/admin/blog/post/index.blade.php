@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')

<main class="w-full flex-grow p-3">
    <x-admin.layout.indexnav name=post title=POST button='normal' gate='true' />
       <!--Card-->
       <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
          <table id="ajaxpost" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
             <thead class="bg-blue-600 text-white">
                <tr>
                   <th data-priority="1">ID</th>
                   <th data-priority="2">TITLE</th>
                   <th data-priority="3">CREATED BY</th>
                   <th data-priority="4">CREATED AT</th>
                   <th data-priority="5">UPDATED AT</th>
                   <th data-priority="6">ACTION</th>
                </tr>
             </thead>
          </table>
       </div>
       <!--/Card-->
 </main>

@endsection 
@section('footerSection')
    <script type="text/javascript">
    $(document).ready(function() {
        $('#ajaxpost').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        "ajax": {
            "url": '{{ route('ajaxpost') }}',
            'headers': {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            "type": "POST"
        },
        columns: [
            { data: 'uniqid', name: 'uniqid' },
            { data: 'title', name: 'title' },
            { data: 'created_by', name: 'created_by' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'action', name: 'action',  orderable: false, searchable: false },
        ]
        })
        .columns.adjust()
        .responsive.recalc();
        });
    </script>
@endsection