<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "title", 'name' => "TITLE", 'required' => true])
   </div>
   <div class="md:w-10/12">
      {{ Form::text('title',$post->title ,array('id'=>'title','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "title"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "subtitle", 'name' => "SUB TITLE", 'required' => false])
   </div>
   <div class="md:w-10/12">
      {{ Form::text('subtitle',$post->subtitle ,array('id'=>'','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "subtitle"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "postslug", 'name' => "SLUG", 'required' => true]) 
   </div>
   <div class="md:w-10/12">
      {{ Form::text('postslug',$post->postslug ,array('id'=>'','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }} 
      @include('helper.formerror', ['error' => "postslug"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "keyword", 'name' => "SEO KEYWORD", 'required' => true]) 
   </div>
   <div class="md:w-10/12">
      {{ Form::text('keyword',$post->keyword ,array('id'=>'','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "keyword"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "description", 'name' => "SEO DESCRIPTION", 'required' => true]) 
   </div>
   <div class="md:w-10/12">
      {{ Form::text('description',$post->description ,array('id'=>'','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "description"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-1/6">
      @include('helper.formlabel', ['for' => "category", 'name' => "CATEGORY", 'required' => true]) 
   </div>
   <div class="md:w-2/6">
      <select id="category" name="category_id" class="form-select block w-full focus:bg-white  p-1" >
         <option value="" selected disabled>Select</option>
         @foreach($category as $key => $eachcategory)
         <option value="{{ $key }}" {{ ($post->category_id == $key) ? 'selected' : '' }}>{{ $eachcategory }}</option>
         @endforeach
      </select>
      @include('helper.formerror', ['error' => "category_id"])
   </div>
   <div class="md:w-1/6">
      @include('helper.formlabel', ['for' => "subcategory_id", 'name' => "SUB CATEGORY", 'required' => true]) 
   </div>
   <div class="md:w-2/6">
      <select name="subcategory_id" id="subcategory_id" class="form-select block w-full focus:bg-white  p-1">
      </select>
      @include('helper.formerror', ['error' => "subcategory_id"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "tagoption", 'name' => "TAG", 'required' => false]) 
   </div>
   <div class="md:w-4/12">
      {{-- <input class="form-input rounded block w-full p-1 focus:bg-white" id="my-textfield" type="text" value=""> --}}
      <select name="tag_select[]" id="tagoption" class="form-select block w-full focus:bg-white dynamic  js-example-basic-multiple_two" multiple="multiple">
         <option value="">Select Tag</option>
      </select>
      @include('helper.formerror', ['error' => "tag_select"])
   </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "image", 'name' => "BANNER IMAGE", 'required' => false]) 
   </div>
   <div class="md:w-4/12">
      <input type="file" name="image"  accept="image/*" id="image" class="form-input rounded block w-full p-1 focus:bg-white"> 
      @include('helper.formerror', ['error' => "image"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-1/6">
      @include('helper.formlabel', ['for' => "videolink", 'name' => "VIDEO LINK", 'required' => false])  
   </div>
   <div class="md:w-2/6">
      {{ Form::text('video_link',$post->video_link ,array('placeholder'=>'Youtube Link','id'=>'videolink','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "video_link"])
   </div>
   <div class="md:w-1/6">
      {{-- @include('helper.formlabel', ['for' => "videostatus", 'name' => "VIDEO STATUS", 'required' => false])   --}}
      <label class="block text-gray-600 font-bold md:text-left mb-3 md:mb-0 ml-1 md:ml-10" for='videostatus'>
         VIDEO STATUS
     </label>
   </div>
   <div class="md:w-2/2">
      {!! Form::checkbox('video_status',  null,  isset($post) ? $post->video_status : 0 ,array('id'=>'videostatus','class'=>'form-checkbox h-5 w-5 text-green-600')) !!}
      @include('helper.formerror', ['error' => "video_status"])
   </div>
   <div class="md:w-1/6">
      {{-- @include('helper.formlabel', ['for' => "publish", 'name' => "PUBLISH", 'required' => false]) --}}
      <label class="block text-gray-600 font-bold md:text-left mb-3 md:mb-0 ml-1 md:ml-10" for='publish'>
         PUBLISH
     </label>
   </div>
   <div class="md:w-2/2">
      {!! Form::checkbox('publish',  1,  isset($post) ? $post->publish : 0 ,array('id'=>'publish','class'=>'form-checkbox h-5 w-5 text-green-600')) !!}
      @include('helper.formerror', ['error' => "publish"])
   </div>

   <div class="md:w-1/6">
      {{-- @include('helper.formlabel', ['for' => "blogcomment", 'name' => "BLOG COMMENT", 'required' => false]) --}}
      <label class="block text-gray-600 font-bold md:text-left mb-3 md:mb-0 ml-1 md:ml-10" for='blogcomment'>
         BLOG COMMENT
     </label>
   </div>
   <div class="md:w-2/2">
      {!! Form::checkbox('blogcomment',  1,  isset($post) ? $post->blogcomment : 0 ,array('id'=>'blogcomment','class'=>'form-checkbox h-5 w-5 text-green-600')) !!}
      @include('helper.formerror', ['error' => "blogcomment"])
   </div>
</div>

<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "country", 'name' => "SELECT COUNTRY", 'required' => false]) 
   </div>
   <div class="md:w-2/12">
      <select id="country" name="country" class="form-select block w-full focus:bg-white p-1" >
         <option value="" selected disabled>Select</option>
         @foreach($countries as $key => $country)
         <option value="{{ $key }}" {{ ($post->country_id == $key) ? 'selected' : '' }}>{{ $country }}</option>
         @endforeach
      </select>
      @include('helper.formerror', ['error' => "country_id"])
   </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "state", 'name' => "SELECT STATE", 'required' => false]) 
   </div>
   <div class="md:w-2/12">
      <select name="state" id="state" class="form-select block w-full focus:bg-white p-1"> </select>
      @include('helper.formerror', ['error' => "state"])
   </div>
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "city", 'name' => "SELECT CITY", 'required' => false]) 
   </div>
   <div class="md:w-2/12">
      <select name="city" id="city" class="form-select block w-full focus:bg-white p-1"> </select>
      @include('helper.formerror', ['error' => "city"])
   </div>
</div>

<div class="md:flex mb-6">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "shortdescription", 'name' => "SHORT DESCRIPTION", 'required' => true]) 
   </div>
   <div class="md:w-10/12">
      {{ Form::textarea('interpretation',$post->interpretation ,array('id'=>'shortdescription', 'class'=>'summernote form-textarea block w-full focus:bg-white', 'rows'=>'2')) }}
      @include('helper.formerror', ['error' => "interpretation"])
   </div>
</div>

<div class="md:flex mb-6">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "description", 'name' => "DESCRIPTION", 'required' => true]) 
   </div>
   <div class="md:w-10/12">
      {{ Form::textarea('body',$post->body ,array('id'=>'description','class'=>'summernote_two form-textarea block w-full focus:bg-white', 'rows'=>'10')) }}
      {{-- <textarea id="editor1" name="editor1" rows="7" class="form-control ckeditor" placeholder="Write your message.."></textarea> --}}
      @include('helper.formerror', ['error' => "body"])
   </div>
</div>

<div class="md:flex mb-6">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "comment", 'name' => "COMMENTS", 'required' => false]) 
   </div>
   <div class="md:w-10/12">
      {{ Form::textarea('comments',$post->comments ,array('id'=>'comment','class'=>'form-textarea block w-full focus:bg-white p-1', 'rows'=>'2')) }}
      @include('helper.formerror', ['error' => "comments"])
   </div>
</div>
