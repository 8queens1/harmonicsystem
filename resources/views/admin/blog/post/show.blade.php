@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='post' title="POST - {{ $post->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $post->uniqid }}" />
         <x-admin.layout.showlabeldetails title='TITLE' value="{{ $post->title }}" />
      </div>
      <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='SUB TITLE' value="{{ $post->subtitle }}" />
        <x-admin.layout.showlabeldetails title='SLUG' value="{{ $post->postslug }}" />
     </div>
     <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='SEO KEYWORD' value="{{ $post->keyword }}" />
        <x-admin.layout.showlabeldetails title='SEO DESCRIPTION' value="{{ $post->description }}" />
     </div>
     <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='CATEGORY' value="{{ $post->category }}" />
        <x-admin.layout.showlabeldetails title='SUB CATEGORY' value="{{ $post->subcategory   }}" />
     </div>
     <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='TAG' value="{{ $post->tags->pluck('name')->implode(', ') }}" />
        <x-admin.layout.showlabeldetails title='VIDEO STATUS' value="{{ isset($post) && $post->video_status  ? 'YES' : 'NO' }}" />
      </div>

      <div class="md:flex mb-3">
         @if($post->video_status)
         <div class="md:w-2/12">
            <label class="block text-gray-800 font-bold md:text-left mb-3 md:mb-0 ml-1">
               BANNER <span class="float-right text-dark">:</span>
            </label> 
         </div>
         <div class="md:w-4/12">
            <label class="block text-blue-800 font-bold md:text-left mb-3 md:mb-0 ml-3">
               {{ $post->video_link }}
            </label> 
         </div>
         @elseif(isset($post->image))
         <div class="md:w-2/12">
            <label class="block text-gray-800 font-bold md:text-left mb-3 md:mb-0 ml-1">
               BANNER <span class="float-right text-dark">:</span>
            </label> 
         </div>
         <div class="md:w-4/12">
            <label class="block text-blue-800 font-bold md:text-left mb-3 md:mb-0 ml-3">
               <img alt="Placeholder" class="block h-auto md:h-64 w-full" src="{{ url('/images/blog/images/'.$post->image) }}">
            </label> 
         </div>

         @else
         <div class="md:w-2/12">
            <label class="block text-gray-800 font-bold md:text-left mb-3 md:mb-0 ml-1">
               BANNER <span class="float-right text-dark">:</span>
            </label> 
         </div>
         <div class="md:w-4/12">
            <label class="block text-blue-800 font-bold md:text-left mb-3 md:mb-0 ml-3">
               NO BANNER
            </label> 
         </div>
         @endif

        <x-admin.layout.showlabeldetails title='BLOG PUBLISH' value="{{ isset($post) && $post->publish  ? 'YES' : 'NO' }}" />
      </div>
      
      
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='BLOG COMMENT' value="{{ isset($post) && $post->blogcomment  ? 'YES' : 'NO' }}" />
         @if($post->country)
         <nav class>
         <ol class="list-reset flex text-grey-dark">
            <li class="font-bold">COUNTRY<span class="text-blue-800 ml-20"> {{ $post->country }}</span></li>
            @if($post->state)
            <li><span class="ml-2"></span></li>
            <li class="font-bold">STATE: <span class="text-blue-800"> {{ $post->state }}</span></li>
            @endif
            @if($post->city)
            <li><span class="ml-2"></span></li>
            @endif
            <li class="font-bold">CITY: <span class="text-blue-800"> {{ $post->city }}</span></li>
         </ol>
         </nav>
         @else
         <li class="font-bold">COUNTRY  <span class="text-blue-800"> </span></li>  
         @endif
     </div>


     {{-- @if($post->country)
<div class="md:flex mb-3">

    <div class="md:w-2/12">
       <label class="block text-gray-800 font-bold md:text-left mb-3 md:mb-0 ml-1">
          COUNTRY <span class="float-right text-dark">:</span>
       </label> 
    </div>
    <div class="md:w-2/12">
       <label class="block text-blue-800 font-bold md:text-left mb-3 md:mb-0 ml-3">
          {{ $post->country }}
       </label> 
    </div>

    @if($post->state)
    <div class="md:w-2/12">
       <label class="block text-gray-800 font-bold md:text-left mb-3 md:mb-0 ml-1">
           STATE <span class="float-right text-dark">:</span>
       </label> 
    </div>
    <div class="md:w-2/12">
       <label class="block text-blue-800 font-bold md:text-left mb-3 md:mb-0 ml-3">
          {{ $post->state }}
       </label> 
    </div>
    @endif

    @if($post->city)
    <div class="md:w-2/12">
       <label class="block text-gray-800 font-bold md:text-left mb-3 md:mb-0 ml-1">
           CITY <span class="float-right text-dark">:</span>
       </label> 
    </div>
    <div class="md:w-2/12">
       <label class="block text-blue-800 font-bold md:text-left mb-3 md:mb-0 ml-3">
          {{ $post->city }}
       </label> 
    </div>
    @endif
 </div> 
 @endif--}}




        <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='SHORT DESCRIPTION' value="{!! $post->interpretation !!}" />
         <x-admin.layout.showlabeldetails title='DESCRIPTION' value="{!! $post->body !!}" />
        </div>
        <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $post->comments }}" />
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $post->created_by }}" />
        </div>
        <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $post->created_at }}" />
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $post->updated_at }}" />
        </div>


   </div>
</main>
@endsection
@section('footerSection')
@endsection