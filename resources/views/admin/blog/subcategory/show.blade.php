@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='subcategory' title="SUB CATEGORY - {{ $subcategory->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $subcategory->uniqid }}" />
         <x-admin.layout.showlabeldetails title='SUB CATEGORY NAME' value="{{ $subcategory->name }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='STATUS' value="{{ isset($subcategory) && $subcategory->active ? 'Active' : 'In Active' }}" />
         <x-admin.layout.showlabeldetails title='SLUG' value="{{ $subcategory->slug }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='SEO KEYWORD' value="{{ $subcategory->keyword }}" />
         <x-admin.layout.showlabeldetails title='SEO DESCRIPTION' value="{{ $subcategory->description }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $subcategory->comments }}" />
         <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $subcategory->created_by }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $subcategory->created_at }}" />
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $subcategory->updated_at }}" />
      </div>
   </div>
</main>
@endsection
@section('footerSection')
@endsection