@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')

<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.createoreditnav  name=subcategory title="SUB CATEGORY" :obj="$subcategory" backbutton="enable" />
    <!--Card-->
    <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
       {!! Form::model($subcategory, ['route' => ['subcategory.store', $subcategory->id],  'id' => '', 'class' => ' form_prevent_multiple_submits ', 'novalidate' => 'novalidate', 'files' => 'true','enctype'=>'multipart/form-data']) !!}
       {{ Form::hidden('id', $subcategory->id, array('id' => 'invisible_id')) }}
       {{ Form::hidden('uniqid', $subcategory->uniqid, array('id' => 'invisible_id')) }}

        @include('admin.blog.subcategory.form') 

        <div class="md:flex md:items-center justify-center">
         @if ($subcategory->id)
         {!! Form::button( '<i class="fa fa-spinner m-1 fa-spin"></i> UPDATE', ['type' => 'submit', 'class' => 'shadow bg-green-500 hover:bg-green-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 m-1 flex rounded button_prevent_multiple_submits'] ) !!}
         @else
         {!! Form::button( '<i class="fa fa-spinner m-1 fa-spin"></i> CREATE', ['type' => 'submit', 'class' => 'shadow bg-green-500 hover:bg-green-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 m-1 flex rounded button_prevent_multiple_submits'] ) !!}
         @endif
         <a href="" class="shadow bg-gray-500 hover:bg-gray-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">CANCEL</a>
       </div>

       {!! Form::close() !!}
    </div>
 </main>

@endsection
@section('footerSection')
@endsection