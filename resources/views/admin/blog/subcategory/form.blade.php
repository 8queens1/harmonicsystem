<div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "name", 'name' => "SUB CATEGORY NAME", 'required' => true])  
     </div>
     <div class="md:w-3/12">
        {{ Form::text('name',$subcategory->name ,array('id'=>'name', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
         @include('helper.formerror', ['error' => "name"])
     </div>

  <div class="md:w-1/12">
   @include('helper.formlabel', ['for' => "active", 'name' => "STATUS", 'required' => true])  
  </div>
  <div class="md:w-1/12 pt-1">
      {!! Form::checkbox('active',  null,  isset($subcategory) ? $subcategory->active : 0 ,array('id'=>'active','class'=>'form-checkbox h-5 w-5 text-green-600')) !!}
      @include('helper.formerror', ['error' => "active"])
  </div>
</div>


<div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "slug", 'name' => "SLUG", 'required' => true]) 
    </div>
    <div class="md:w-10/12">
       {{ Form::text('slug',$subcategory->slug ,array('id'=>'slug','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }} 
        @include('helper.formerror', ['error' => "slug"])
    </div>
 </div>

 <div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "keyword", 'name' => "SEO KEYWORD", 'required' => true]) 
    </div>
    <div class="md:w-10/12">
       {{ Form::text('keyword',$subcategory->keyword ,array('id'=>'keyword','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
        @include('helper.formerror', ['error' => "keyword"])
    </div>
 </div>

 <div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "description", 'name' => "SEO DESCRIPTION", 'required' => true]) 
    </div>
    <div class="md:w-10/12">
        {{ Form::textarea('description',$subcategory->description ,array('id'=>'description','class'=>'p-1 form-textarea block w-full focus:bg-white', 'rows'=>'2')) }}
        @include('helper.formerror', ['error' => "description"])
    </div>
 </div>


 <div class="md:flex mb-6">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "comments", 'name' => "REMARKS (Optional)", 'required' => false]) 
    </div>
    <div class="md:w-10/12">
       {{ Form::textarea('comments',$subcategory->comment ,array('id'=>'comments','class'=>'p-1 form-textarea block w-full focus:bg-white', 'rows'=>'2')) }}
        @include('helper.formerror', ['error' => "comments"])
    </div>
 </div>
