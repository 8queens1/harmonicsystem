<div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "name", 'name' => "CATEGORY NAME", 'required' => true]) 
     </div>
     <div class="md:w-3/12">
        {{ Form::text('name',$category->name ,array('id'=>'name', 'class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
        @include('helper.formerror', ['error' => "name"])
     </div>

     <div class="md:w-2/12 pl-6">
      @include('helper.formlabel', ['for' => "categoryoption", 'name' => "SUB CATEGORY", 'required' => true]) 
     </div>
     <div class="md:w-3/12">
        <select name="category_select[]" id="categoryoption" value="{{  $category->categorySelect }}" class="form-select block w-full focus:bg-white  p-1 dynamic  js-example-basic-multiple_one" multiple="multiple">
            <option value="">Select Category</option>
         </select>
        @include('helper.formerror', ['error' => "category_select"])
     </div>

  <div class="md:w-1/12">
   @include('helper.formlabel', ['for' => "active", 'name' => "STATUS", 'required' => true]) 
  </div>
  <div class="md:w-1/12 pt-1">
      {!! Form::checkbox('active',  null,  isset($category) ? $category->active : 0 ,array('id'=>'active','class'=>'form-checkbox h-5 w-5 text-green-600')) !!}
     @include('helper.formerror', ['error' => "active"])
  </div>
</div>


<div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "slug", 'name' => "SLUG", 'required' => true])
    </div>
    <div class="md:w-10/12">
       {{ Form::text('slug',$category->slug ,array('id'=>'slug','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }} 
       @include('helper.formerror', ['error' => "slug"])
    </div>
 </div>

 <div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "keyword", 'name' => "SEO KEYWORD", 'required' => true])
    </div>
    <div class="md:w-10/12">
       {{ Form::text('keyword',$category->keyword ,array('id'=>'keyword','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
       @include('helper.formerror', ['error' => "keyword"])
    </div>
 </div>

 <div class="md:flex mb-3">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "description", 'name' => "SEO DESCRIPTION", 'required' => true])
    </div>
    <div class="md:w-10/12">
        {{ Form::textarea('description',$category->description ,array('id'=>'description','class'=>'summernote form-textarea block w-full focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "description"])
    </div>
 </div>


 <div class="md:flex mb-6">
    <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "comments", 'name' => "REMARKS (Optional)", 'required' => false])
    </div>
    <div class="md:w-10/12">
       {{ Form::textarea('comments',$category->comment ,array('id'=>'comments','class'=>'form-textarea block w-full focus:bg-white', 'rows'=>'2')) }}
       @include('helper.formerror', ['error' => "comments"])
    </div>
 </div>
