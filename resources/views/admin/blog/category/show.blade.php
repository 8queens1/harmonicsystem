@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')
<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.shownav  name='category' title="CATEGORY - {{ $category->uniqid }}" backbutton="enable" />
   <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white container mx-auto">
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='ID' value="{{ $category->uniqid }}" />
         <x-admin.layout.showlabeldetails title='CATEGORY NAME' value="{{ $category->name }}" />
      </div>
      <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='SUB CATEGORY NAME' value="{{ $category->subcategories->pluck('name')->implode(', ') }}" />
         <x-admin.layout.showlabeldetails title='STATUS' value="{{ isset($category) && $category->active  ? 'Active' : 'In Active' }}" />
        </div>
        <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='SLUG' value="{{ $category->slug }}" />
         <x-admin.layout.showlabeldetails title='SEO KEYWORD' value="{{ $category->keyword }}" />
        </div>
        <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='SEO DESCRIPTION' value="{{ $category->description }}" />
         <x-admin.layout.showlabeldetails title='REMARKS' value="{{ $category->comments }}" />
        </div>
        <div class="md:flex mb-3">
        <x-admin.layout.showlabeldetails title='CREATED BY' value="{{ $category->created_by }}" />
         <x-admin.layout.showlabeldetails title='CREATED AT' value="{{ $category->created_at }}" />
        </div>
        <div class="md:flex mb-3">
         <x-admin.layout.showlabeldetails title='UPDATED AT' value="{{ $category->updated_at }}" />
      </div>

   </div>
</main>
@endsection
@section('footerSection')
@endsection