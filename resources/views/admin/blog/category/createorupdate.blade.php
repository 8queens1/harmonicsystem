@extends('components.admin.layout.app')
@section('headSection')
@endsection
@section('main-content')

<main class="w-full flex-grow px-6 py-2">
   <x-admin.layout.createoreditnav  name=category title="CATEGORY" :obj="$category" backbutton="enable" />
    <!--Card-->
    <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
       {!! Form::model($category, ['route' => ['category.store', $category->id],  'id' => '', 'class' => ' form_prevent_multiple_submits ', 'novalidate' => 'novalidate', 'files' => 'true','enctype'=>'multipart/form-data']) !!}
       {{ Form::hidden('id', $category->id, array('id' => 'invisible_id')) }}
       {{ Form::hidden('uniqid', $category->uniqid, array('id' => 'invisible_id')) }}

        @include('admin.blog.category.form') 

        <div class="flex md:items-center justify-center">
         @if ($category->id)
         {!! Form::button( '<i class="fa fa-spinner m-1 fa-spin"></i> UPDATE', ['type' => 'submit', 'class' => 'shadow bg-green-500 hover:bg-green-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 m-1 flex rounded button_prevent_multiple_submits'] ) !!}
         @else
         {!! Form::button( '<i class="fa fa-spinner m-1 fa-spin"></i> CREATE', ['type' => 'submit', 'class' => 'shadow bg-green-500 hover:bg-green-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 m-1 flex rounded button_prevent_multiple_submits'] ) !!}
         @endif
         <a href="" class="shadow bg-gray-500 hover:bg-gray-600 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">CANCEL</a>
       </div>

       {!! Form::close() !!}
    </div>
 </main>

@endsection
@section('footerSection')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('.js-example-basic-multiple_one').select2();
ajaxsubcategory();
function ajaxsubcategory() {
  $.ajax({
      url: "{{route('ajaxsubcategory')}}",
      mehtod: "get",
      dataType: 'json',
      success: function(data) {
          $('#categoryoption').html(data.categoryoption);
         $('#categoryoption').val({!! $category->categorySelect !!});
         $('#categoryoption').trigger('change');
      }
  })
}
});
</script>
@endsection