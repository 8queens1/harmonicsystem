<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "title", 'name' => "TITLE", 'required' => true])
   </div>
   <div class="md:w-6/12">
      {{ Form::text('title',$aboutme->title ,array('placeholder'=>'Youtube Link','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "title"])
   </div>

   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "active", 'name' => "ABOUT ME ACTIVE", 'required' => true])
   </div>
   <div class="md:w-2/12">
      {!! Form::checkbox('active',  null,  isset($aboutme) ? $aboutme->active : 0 ,array('id'=>'','class'=>'form-checkbox h-5 w-5 text-green-600 mt-1')) !!}
      @include('helper.formerror', ['error' => "active"])
   </div>
</div>

<div class="md:flex mb-6">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "avatar", 'name' => "AVATAR", 'required' => true])
   </div>
   <div class="md:w-5/12">
      <input type="file" name="avatar"  accept="image/*" id="avatar" class="form-input rounded block w-full p-1 focus:bg-white"> 
      @include('helper.formerror', ['error' => "avatar"])
   </div>

   @if($aboutme->avatar)
   <div class="md:w-1/12 pl-1">
      <div class="realtive z-10 w-16 h-16 rounded-full overflow-hidden border-4 border-gray-400 hover:border-gray-300 focus:border-gray-300 focus:outline-none">
         <img src="{{ URL('/avatar/thumbnail/'.  $aboutme->avatar) }}">
      </div>
   </div>
   @else
   <div class="md:w-1/12 pl-1">
   </div>
   @endif
   
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "avataractive", 'name' => "AVATAR ACTIVE", 'required' => true])
   </div>
   <div class="md:w-1/12">
      {!! Form::checkbox('avataractive',  null,  isset($aboutme) ? $aboutme->avataractive : 0 ,array('id'=>'','class'=>'form-checkbox h-5 w-5 text-green-600 mt-1')) !!}
      @include('helper.formerror', ['error' => "avataractive"])
   </div>


   
</div>



<div class="md:flex mb-3">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "avatartaglineone", 'name' => "AVATAR TAG LINE ONE", 'required' => true])
   </div>
   <div class="md:w-4/12">
      {{ Form::text('avatartaglineone',$aboutme->avatartaglineone ,array('placeholder'=>'eg: name','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "avatartaglineone"])
   </div>

   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "avatartaglinetwo", 'name' => "AVATAR TAG LINE TWO", 'required' => true])
   </div>
   <div class="md:w-4/12">
      {{ Form::text('avatartaglinetwo',$aboutme->avatartaglinetwo ,array('placeholder'=>'eg: designation','class'=>'form-input rounded block w-full p-1 focus:bg-white')) }}
      @include('helper.formerror', ['error' => "avatartaglinetwo"])
   </div>
</div>



<div class="md:flex mb-6">
   <div class="md:w-2/12">
      @include('helper.formlabel', ['for' => "body", 'name' => "BODY", 'required' => true])
   </div>
   <div class="flex md:w-10/12">
      {{ Form::textarea('body',$aboutme->body ,array('id'=>'','class'=>'summernote p-1 form-textarea block w-full focus:bg-white', 'rows'=>'5')) }}
      @include('helper.formerror', ['error' => "body"])
   </div>
</div>