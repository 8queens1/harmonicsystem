@component('mail::message')
# New Enquiry 

First Name: <b>{{ $fname }}</b>

Last Name: <b>{{ $lname ?? '' }}</b>

Email: {{ $email }}<br>

<br>
Message: <b>{{ $message }}</b>

Thanks,<br> 
{{ App::make('configuration')->company_name }}
@endcomponent
