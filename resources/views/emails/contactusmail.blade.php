@component('mail::message')

# <span style="font-style: italic;">Hi </span> <span style="color: #006064;">{{ $data }}</span>
<b>We will be in touch shortly</b>.

# ORGANIC AND NATURAL PRODUCTS

<hr>
# www.OrgoStore.com
<hr>

# “We deliver all over India”
# “No minimum order restriction”
# Quality Products at Reasonable Price

@component('mail::button', ['url' => 'https://www.orgostore.com/'])
Visit Our Site
@endcomponent


<h1>Contact Us: </h1>
<i style="color:#006064; ">
<b>{{ App::make('configuration')->company_name }}<br>
	 Email : {{ App::make('configuration')->email }}</b> 
</i>
<hr>

Thanks,<br>
{{ App::make('configuration')->company_name }}
@endcomponent
