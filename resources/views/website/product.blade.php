@extends('components.website.layout.app')
@section('headSection')
@endsection
@section('main-content')

<br>
<section class="w-10/12 mx-auto rounded-2xl text-gray-600 body-font border-black shadow-2xl bg-white md:max-w-screen">
     <div class="container mx-auto flex  md:flex-row flex-col items-center">
          <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
               <img class="mx-auto object-cover object-center rounded" alt="hero" src="{{ asset('images/ps.jpg') }}">
          </div>
          <div
               class="m-5 lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
               <h1 class="font-family:arial sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Power Supply </h1>
               <p class="mb-8 leading-relaxed font-semibold">
                    ➼ Linear Power Supply - cum - Battery Charger<br>
                    ➼ Linear Power Supply - cum - Battery Charge Controller<br>
                    ➼ Switch mode Power Supply (SMPS)<br>
                    ➼ Un Interpreted Power Supply (UPS)<br>
                    ➼ Solar Panel / Battery Charge Controller<br>
                    ➼ Invertors / DC to DC Convertors
               </p>
          </div>

     </div>
</section><br>
<section class="w-10/12 mx-auto rounded-2xl text-gray-600 body-font border-black shadow-2xl bg-white md:max-w-screen">
     <div class="container mx-auto flex  md:flex-row flex-col items-center">
          <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
               <img class="mx-auto object-cover object-center rounded" alt="hero" src="{{ asset('images/cap.jpg') }}">
          </div>
          <div
               class="m-5 lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
               <h1 class="font-family:arial sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Communication
                    Equipments</h1>
               <p class="mb-8 leading-relaxed font-semibold">
                    ➼ Repeater Enclosure with Power Supply & Rick<br>
                    ➼ Radio Interface Communication Kit (RICK)<br>
                    ➼ Radio Programming Kits<br>
                    ➼ DTC Enclosure with Power Supply (19 Inch RICK Power Supply Modules<br>
                    ➼ Industrial Public Address Systems<br>
                    ➼ Power over Ethernet for WAN modem
               </p>
          </div>

     </div>
</section><br>
<section class="w-10/12 mx-auto rounded-2xl text-gray-600 body-font border-black shadow-2xl bg-white md:max-w-screen">
     <div class="container mx-auto flex  md:flex-row flex-col items-center">
          <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
               <img class="mx-auto my-auto object-center rounded" alt="hero" src="{{ asset('images/ip.jpg') }}">
          </div>
          <div
               class="m-5 lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
               <h1 class="font-family:arial sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Industrial Equipments
               </h1>
               <p class="mb-8 leading-relaxed font-semibold">
                    ➼ CNC System & Drives<br>
                    ➼ PLC Modules<br>
                    ➼ Micro Processors / Micro Controller<br>
                    ➼ Micro Controller Based Equipment
               </p>
          </div>

     </div>
</section><br>

<section class="text-gray-900 body-font">
     <div class="container px-5 py-24 mx-auto">
          <div class="flex flex-col">
               <div class="h-1 bg-white rounded overflow-hidden">
                    <div class="w-24 h-full bg-indigo-500"></div>
               </div>
               <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
                    <h1 class="sm:w-2/5 text-gray-900 font-medium title-font text-2xl mb-2 sm:mb-0">Harmonic System
                         Producsts and Services</h1>
                    <p class="sm:w-3/5 leading-relaxed font-bold sm:pl-10 pl-0">Harmonic System have 21Years of
                         experience over the same fields with more happy Customers all over india</p>
               </div>
          </div>
          <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
               <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                    <div class="rounded-lg h-auto overflow-hidden">
                         <img alt="content" class="object-cover object-center h-full w-full"
                              src="{{ asset('images/newps.jpg') }}">
                    </div>
               </div>
               <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                    <div class="rounded-lg h-auto overflow-hidden">
                         <img alt="content" class="object-cover object-center h-full w-full"
                              src="{{ asset('images/newcom.jpg') }}">
                    </div>
               </div>
               <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                    <div class="rounded-lg h-auto overflow-hidden">
                         <img alt="content" class="object-cover object-center h-full w-full"
                              src="{{ asset('images/newip.jpg') }}">
                    </div>
               </div>
          </div>
     </div>
</section>




<br>
<section class="text-gray-900 body-font">
     <div class="container px-5 mx-auto">
          <div class="flex flex-wrap w-full mb-20">
               <div class="lg:w-1/2 w-full mb-6 lg:mb-0">
                    <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">Power Supply Units</h1>
                    <div class="h-1 w-20 bg-indigo-500 rounded"></div>
               </div>
               <p class="lg:w-1/2 w-full font-bold leading-relaxed text-black-900">A power supply unit (PSU) converts
                    mains AC to
                    low-voltage regulated DC power for the internal components of a computer. Modern personal computers
                    universally use switched-mode power supplies. Some power supplies have a manual switch for selecting
                    input voltage, while others automatically adapt to the mains voltage.</p>
          </div>
          <div class="flex flex-wrap -m-4">
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/psu1.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/psu2.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/psu3.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/psu4.jpg') }}" alt="content">
                    </div>
               </div>
          </div>
          <div class="flex flex-wrap -m-4">
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/psnew1.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/psnew2.jpg') }}" alt="content">
                    </div>
               </div>
          </div>
     </div>
</section>
<br>
<br>
<section class="text-gray-900 body-font">
     <div class="container px-5 mx-auto">
          <div class="flex flex-wrap w-full mb-20">
               <div class="lg:w-1/2 w-full mb-6 lg:mb-0">
                    <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">Harmonic Custom Circuit
                         boards</h1>
                    <div class="h-1 w-20 bg-indigo-500 rounded"></div>
               </div>
               <p class="lg:w-1/2 w-full font-bold leading-relaxed text-black-900">Harmonic System Provides best in
                    class custom
                    Circuit Board Chips. We have 21Years of experience on making custom Circuit boards</p>
          </div>
          <div class="flex flex-wrap -m-4">
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps1.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps2.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps3.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps4.jpg') }}" alt="content">
                    </div>
               </div>
          </div>
          <div class="flex flex-wrap -m-4">
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps5.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps6.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps7.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/ps8.jpg') }}" alt="content">
                    </div>
               </div>
          </div>
     </div>
</section>
<br>
<br>
<section class="text-gray-900 body-font">
     <div class="container px-5 mx-auto">
          <div class="flex flex-wrap w-full mb-20">
               <div class="lg:w-1/2 w-full mb-6 lg:mb-0">
                    <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">Digital Repeators</h1>
                    <div class="h-1 w-20 bg-indigo-500 rounded"></div>
               </div>
               <p class="lg:w-1/2 w-full font-bold leading-relaxed text-black-900">In digital communication systems, a
                    repeater is a device that receives a digital signal on an electromagnetic or optical transmission
                    medium and regenerates the signal along the next leg of the medium</p>
          </div>
          <div class="flex flex-wrap -m-4">
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/dp1.jpg') }}" alt="content">
                    </div>
               </div>
               <div class="xl:w-1/4 md:w-1/2 p-4">
                    <div class="bg-gray-100 p-6 rounded-lg">
                         <img class="h-40 rounded w-full object-cover object-center mb-6"
                              src="{{ asset('images/dp2.jpg') }}" alt="content">
                    </div>
               </div>
          </div>
     </div>
</section>

<br>
<section class="text-gray-600 body-font">
     <div class="w-10/12 container px-5 py-8 mx-auto md:max-w-screen">
          <div class="flex flex-col text-center w-full mb-20">
               <p class="font-semibold text-2xl text-black lg:w-2/3 mx-auto leading-relaxed">POWER SUPPLY PRODUCTS
                    <br><span class="inline-block h-1 w-11 rounded bg-indigo-500 mt-8 mb-6"></span>
               </p>
          </div>
          <div class="flex flex-wrap -m-6">
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/battery.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Linear Power Supply - cum -
                                   Battery
                                   Charger</h2>
                              <h3 class="text-gray-500 mb-3">Power Supply Products</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/battery2.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Linear Power Supply - cum -
                                   Battery
                                   Charge Controller</h2>
                              <h3 class="text-gray-500 mb-3">Power Supply Products</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/smpa.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Switch mode Power Supply (SMPS)
                              </h2>
                              <h3 class="text-gray-500 mb-3">Power Supply Products</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/solar.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900"> Solar Panel inverters / Battery
                                   Charge Controller
                              </h2>
                              <h3 class="text-gray-500 mb-3">Power Supply Products</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/ac.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Inverters / (AC to DC Convertors)
                              </h2>
                              <h3 class="text-gray-500 mb-3">Power Supply Products</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>

<section class="text-gray-600 body-font">
     <div class="w-10/12 container px-5 py-8 mx-auto md:max-w-screen">
          <div class="flex flex-col text-center w-full mb-20">
               <p class="font-semibold text-black lg:w-2/3 mx-auto leading-relaxed text-2xl">COMMUNICATION PRODUCTS
                    <br><span class="inline-block h-1 w-11 rounded bg-indigo-500 mt-8 mb-6"></span>
               </p>
          </div>
          <div class="flex flex-wrap -m-6">
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/radio.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Radio Interface Communication Kit
                                   (RICK)</h2>
                              <h3 class="text-gray-500 mb-3">COMMUNICATION PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/repeater.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Repeater Enclosure with Power
                                   Supply &
                                   Rick</h2>
                              <h3 class="text-gray-500 mb-3">COMMUNICATION PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/motokit.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900"> Radio Programming Kits for
                                   MOTOROLA/
                                   KENWOOD etc
                              </h2>
                              <h3 class="text-gray-500 mb-3">COMMUNICATION PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/dcen.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">DTC Enclosure with Power Supply
                                   (19
                                   Inch RICK Power Supply Modules</h2>
                              <h3 class="text-gray-500 mb-3">COMMUNICATION PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/modem.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900"> Industrial Public Address
                                   Systems q
                                   Power over Ethernet for WAN modem
                              </h2>
                              <h3 class="text-gray-500 mb-3">COMMUNICATION EQUIPMENTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl ">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/ac2dc.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Inverters / (AC to DC Convertors)
                              </h2>
                              <h3 class="text-gray-500 mb-3">COMMUNICATION PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>
<section class="text-gray-600 body-font">
     <div class="w-10/12 container px-5 py-8 mx-auto md:max-w-screen">
          <div class="flex flex-col text-center w-full mb-20">
               <p class="font-semibold text-black lg:w-2/3 mx-auto leading-relaxed text-2xl">INDUSTRIAL PRODUCTS
                    <br><span class="inline-block h-1 w-11 rounded bg-indigo-500 mt-8 mb-6"></span>
               </p>
          </div>
          <div class="flex flex-wrap -m-6">
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/trans.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Tranfomer</h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/cns.png') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">CNC System & Drives</h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/plc.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900"> PLC Modules
                              </h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/micro.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">MicroController Based Equipment
                              </h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-indigo-200 hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/solar2.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900"> Solar power systems and
                                   Inverters
                              </h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/dcen.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">DTC Enclosure with Power Supply
                                   (19
                                   Inch RICK Power Supply Modules
                              </h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
               <div class="p-4 lg:w-1/2 bg-white hover:shadow-2xl">
                    <div
                         class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                         <img alt="team"
                              class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                              src="{{ asset('images/modem.jpg') }}">
                         <div class="flex-grow sm:pl-8">
                              <h2 class="title-font font-medium text-lg text-gray-900">Industrial Public Address Systems
                                   Power over Ethernet for WAN modem
                              </h2>
                              <h3 class="text-gray-500 mb-3">INDUSTRIAL PRODUCTS</h3>
                              <p class="mb-4">
                              </p>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section><br>

@endsection
@section('footerSection')
<script>
$("#products").addClass("text-gray-900 bg-gray-200");
</script>
@endsection