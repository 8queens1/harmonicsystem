@extends('components.website.layout.app')
@section('headSection')
@endsection
@section('main-content')


<div class="mt-5 sm:px-6 lg:px-8">
     <div
          class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
          <h1>About Harmonic Technologies</h1>
     </div>
     <div class="w-11/12 mx-auto overflow-hidden flex flex-col justify-center md:max-w-screen">
          <div class="mt-4">
               <div
                    class="px-6 py-4 rounded-xl text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700  font-bold uppercase">
                    About Us
               </div>
               <div class="bg-white rounded-lg p-8 flex flex-col text-center w-full">
                    <p class="mx-auto leading-relaxed font-semibold">The Company was incorporated in 2000.
                         The main goal is to Provide Best in class Service & Maintenance all over India and extend our
                         happy
                         <span class="text-xl text-blue-600">Customers</span>,
                    </p>
               </div>
          </div>
     </div><br>
     <div class="w-11/12 mx-auto overflow-hidden flex flex-col justify-center md:max-w-screen">
          <div class="mt-4">
               <div
                    class="px-6 py-4 rounded-xl text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700  font-bold uppercase">
                    Our Goal
               </div>
               <div class="bg-white rounded-lg p-8 flex flex-col text-center w-full">
                    <p class="mx-auto leading-relaxed font-semibold">To Provide Best in class Service & Maintenance all
                         over India and extend our happy
                         <span class="text-xl text-blue-600">Customers</span>,
                    </p>
               </div>
          </div>
     </div><br>
     <div class="w-11/12 mx-auto md:max-w-screen">
          <div
               class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
               <h1>Services We Provide</h1>
          </div>
          <div class="bg-white rounded-lg p-8 flex flex-col text-center w-full">
               <section class="text-gray-600 body-font">
                    <div class="container px-5 mt-6 mx-auto">
                         <div class="flex flex-col text-center w-full mb-10">
                              <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Power
                                   Supply</h1>
                              <p class="lg:w-2/3 mx-auto leading-relaxed text-md">
                                   We Provide
                                   <span class="text-2xl text-blue-600">Industrial Equiments</span> and <span
                                        class="text-2xl text-blue-600">24v Industrial Power Supply Products</span>
                                   with
                                   <span class="text-2xl text-blue-600">Long Term Service</span> with which we have
                                   builed a
                                   perfect trust across our suppliers<br>
                              </p>
                         </div>
                    </div>
                    <div class="container px-5 mx-auto">
                         <div class="flex flex-col text-center w-full mb-10">
                              <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Industrial
                                   Equipments
                              </h1>
                              <p class="lg:w-2/3 mx-auto leading-relaxed text-md"> We Provide
                                   <span class="text-2xl text-blue-600">Industrial Equiments</span>,
                                   <span class="text-2xl text-blue-600">Industrial PCB's</span>,
                                   <span class="text-2xl text-blue-600">Hybrid Modules</span> and
                                   <span class="text-2xl text-blue-600">Motor Drivers</span>
                              </p>
                         </div>
                    </div>
                    <div class="container px-5 mx-auto">
                         <div class="flex flex-col text-center w-full mb-10">
                              <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">UPS</h1>
                              <p class="lg:w-2/3 mx-auto leading-relaxed text-md">
                                   We Provide best in class
                                   <span class="text-2xl text-blue-600">Renewable Energies, Solar charger Control
                                        for UPS,
                                        Windmill Drivers</span> and <span
                                        class="text-2xl text-blue-600">Reverse-Engineering
                                        Drivers</span>
                                   all india with a long term service
                              </p>
                         </div>
                    </div>
               </section>
          </div>
     </div>
</div><br>
@endsection
@section('footerSection')
<script>
$("#harmonic").addClass("text-gray-900 bg-gray-200");
</script>
@endsection