@extends('components.website.layout.app')
@section('headSection')
@endsection
@section('main-content')


<div class="m-5 h-auto flex items-center p-4 lg:justify-center">
     <div
          class="flex flex-col overflow-hidden bg-white rounded-md shadow-xl hover:shadow-2xl max md:flex-row md:flex-1 lg:max-w-screen-md">
          <div
               class="p-4 py-6 text-white bg-gradient-to-r from-blue-900 via-blue-900 to-blue-700 md:w-80 md:flex-shrink-0 md:flex md:flex-col md:items-center md:justify-evenly">
               <div
                    class="text-lg font-semibold tracking-widest text-white uppercase rounded-lg dark-mode:text-white focus:outline-none focus:shadow-outline">
                    Harmonic System
               </div>
               <p class="mt-6 font-semibold text-center text-white md:mt-0">
                    Harmonic System established in 1998, Specialized in manufacturing high performance power supplies
                    for HF
                    & VHF Radio’s, AC - DC Adaptors, DC to DC converters and battery chargers. Specialized in linear &
                    SMPS
                    power supply with various ranges in customized way.
               </p>
               <p class="mt-6 text-sm text-center text-gray-300">
                    Read our <a href="#" class="underline">terms</a> and <a href="#" class="underline">conditions</a>
               </p>
          </div>

          <section class="w-full max-w-2xl px-6 py-4 mx-auto bg-white rounded-md shadow-md dark:bg-gray-800">
               <h2 class="text-3xl font-semibold text-center text-gray-800 dark:text-white">Harmonic Systems<h2>
                         <p class="mt-3 text-center text-gray-600 dark:text-gray-400">Get In touch
                         <p>

                         <div class="grid grid-cols-3 gap-6 mt-3 sm:grid-cols-3 md:grid-cols-3">
                              <a href="#"
                                   class="flex flex-col items-center px-4 py-3 text-gray-700 rounded-md dark:text-gray-200 hover:bg-blue-200 dark:hover:bg-blue-500">
                                   <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                        fill="currentColor">
                                        <path fill-rule="evenodd"
                                             d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z"
                                             clip-rule="evenodd" />
                                   </svg>
                              </a>
                              <a href="tel:+91 98407 34558"
                                   class="flex flex-col items-center px-4 py-3 text-gray-700 rounded-md dark:text-gray-200 hover:bg-blue-200 dark:hover:bg-blue-500">
                                   <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                        fill="currentColor">
                                        <path
                                             d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                                   </svg>
                              </a>

                              <a href="mailto:harmonicsindia@yahoo.co.in"
                                   class="flex flex-col items-center px-4 py-3 text-gray-700 rounded-md dark:text-gray-200 hover:bg-blue-200 dark:hover:bg-blue-500">
                                   <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                        fill="currentColor">
                                        <path
                                             d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                   </svg>
                              </a>
                         </div>

                         <div class="mt-6 ">
                              <div class="items-center -mx-2 md:flex">
                                   <div class="w-full mx-2">
                                        <label for="name"
                                             class="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">Name</label>

                                        <input id="name"
                                             class="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                                             type="text">
                                   </div>

                                   <div class="w-full mx-2 mt-4 md:mt-0">
                                        <label for="email" class=" block mb-2 text-sm font-medium text-gray-600
                                            dark:text-gray-200">E-mail</label>

                                        <input id="email"
                                             class="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"
                                             type="email">
                                   </div>
                              </div>

                              <div class="w-full mt-4">
                                   <label for="message"
                                        class="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200">Message</label>

                                   <textarea id="message"
                                        class="block w-full h-36 px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-500 dark:focus:border-blue-500 focus:outline-none focus:ring"></textarea>
                              </div>

                              <div class="flex justify-center mt-6">
                                   <button
                                        class="px-4 py-2 rounded-xl text-white transition-colors bg-gradient-to-r from-blue-900 via-blue-900 to-blue-700 focus:outline-none focus:bg-gray-600">Send
                                        Message</button>
                              </div>
                         </div>
          </section>
     </div>
</div>
<div class="w-11/12 mx-auto md:max-w-screen">
     <div
          class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
          <h1>Address</h1>
     </div>
     <div class="font-semibold p-8 bg-white border-b border-gray-200 hover:shadow-2xl ">
          <section class="text-gray-600 body-font relative">
               <div class="absolute inset-0 bg-gray-300">
                    <iframe width="100%" height="100%" frameborder="0" marginheight="0" marginwidth="0" title="map"
                         scrolling="no"
                         src="https://maps.google.com/maps?q=harmonic%20system%20chennai&t=&z=13&ie=UTF8&iwloc=&output=embed"
                         style="filter:"></iframe>
               </div>
               <div class="container px-5 py-24 mx-auto flex overflow-hidden">
                    <div
                         class="lg:w-1/3 md:w-1/2 bg-white rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0 relative z-10 shadow-md">
                         <h2 class="text-gray-900 text-lg mb-1 title-font">Chennai</h2>
                         <p class="leading-relaxed mb-5 text-gray-600">#6, Vellalar Street, 3rd Main Road.
                              Ambattur Industrial Estate, chennai-58 Mobile: 9840734558, 9840105883 Fax:
                              91-044-26245975</p>
                    </div>
               </div>
          </section>
     </div>
</div>
</div>
</div><br>
@endsection
@section('footerSection')
<script>
$("#contact").addClass("text-gray-900 bg-gray-200");
</script>
@endsection