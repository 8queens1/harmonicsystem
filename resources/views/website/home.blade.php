@extends('components.website.layout.app')
@section('headSection')
@endsection
@section('main-content')

<section class="w-11/12 mx-auto rounded-lg mt-6 mb-6 md:max-w-screen md:mt-3 text-gray-600 body-font"
     style="background-image: url('./images/banner.jpg');">
     <div class="container px-5 py-10 mx-auto">
          <div class="flex flex-col text-center w-full ">
               <img class="mb-3 w-80 mx-auto h-12" alt="hero" src="{{ asset('images/h2name.png') }}">
               <h1 class="sm:text-3xl text-base font-semibold text-white">We Make Machine</h1><span
                    class="bg-white mx-auto h-1 w-20 block mt-4"></span>
               <br>
               <div class="text-white mx-auto leading-relaxed font-semibold w-10/12">
                    <p>Harmonic System established in 1998, Specialized in manufacturing high performance power
                         supplies
                         for HF, UHF (Ultra High Frequenc) & VHF Radio’s, AC - DC Adaptors, DC to DC converters and
                         battery chargers. Specialized in
                         linear & SMPS power supply with various ranges in customized way.Harmonic systems products are
                         widely used in Wireless communication Equipment like MOTOROLA,
                         KENWOOD, ICOM and other HF, VHF, UHF, SONAR and Marine communication equipment’s.
                    </p>
               </div>
          </div>
          <a href="{{ URL('/aboutus') }}"
               class="w-32 mx:auto flex mx-auto mt-8 text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-sm">More
               Info
          </a>
     </div>
</section>
<div class="relative">
     <div class="container mx-auto px-6 pt-6 pb-12 relative">
          <h3 class="flex flex-col items-center text-4xl text-secondary font-bold mb-12">Our Process <span
                    class="bg-blue-900 h-1 w-20 block mt-4"></span></h3>
          <div class="flex flex-col md:flex-row xl:px-32">
               <div class="flex flex-col items-center md:px-6 lg:px-12">
                    <span class="text-6xl text-blue-900">1</span>
                    <h4 class="font-semibold text-2xl text-secondary mb-2">Analysis</h4>
                    <p class="text-center text-secondary-700 leading-relaxed">We have a experinced team to do R&D for
                         the project with the advantage of long term service</p>
               </div>
               <div class="flex flex-col items-center md:px-6 lg:px-12">
                    <span class="text-6xl text-blue-900">2</span>
                    <h4 class="font-semibold text-2xl text-secondary mb-2">Execution</h4>
                    <p class="text-center text-secondary-700 leading-relaxed">With the solution we have in our hand we
                         excute it perfectly
                         by implementing it into various stages of texting</p>
               </div>
               <div class="flex flex-col items-center md:px-6 lg:px-12">
                    <span class="text-6xl text-blue-900">3</span>
                    <h4 class="font-semibold text-2xl text-secondary mb-2">Success</h4>
                    <p class="text-center text-secondary-700 leading-relaxed">With perfect Analysis and Execution we
                         have a
                         success rate of
                         100% in our field over 21years</p>
               </div>
          </div>
     </div>
</div>
<div id="about-us" class="bg-indigo-200 mt-20 py-12">
     <div class="container mx-auto px-6">
          <div class="flex flex-col md:flex-row">
               <div class="container mx-auto  relative">
                    <div class="flex flex-col text-center w-full mt-5">
                         <p class="text-4xl font-bold text-black lg:w-2/3 mx-auto leading-relaxed">Harmonic System
                              <br><span class="inline-block h-1 w-40 rounded bg-blue-900 mt-8 mb-6"></span>
                         </p>
                    </div>
                    <section class="text-gray-900 body-font">
                         <div class="container px-5 mt-3 mx-auto">
                              <div class="flex flex-col text-center w-full mb-10">
                                   <p class="mx-auto leading-relaxed font-semibold">The Company was incorporated in Aug.
                                        1998 It was sister concern
                                        of Harmonic Technologies Incorporated in 2000. The other Companies in
                                        the group of Harmonic Data Net's.
                                        Harmonic has been manufacturing high performance
                                        communication power supply's products. Our expertise in this specialised
                                        field is unrivalled and has made us the largest products of Communication
                                        accessories In 2002 we commenced Service Network of our product at
                                        Bangalore.<br>
                                        Our power supply have found acceptability by a wide spectrum of
                                        user in Government and Industry
                                   </p>
                              </div>
                              <a href="{{ URL('/service') }}"
                                   class="w-32 mx:auto flex mx-auto mt-8 text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-sm">More
                                   Info</a>
                         </div>
               </div>
          </div>
     </div>
</div>
<div class="relative">
     <div class="container mx-auto px-6 py-20 relative">
          <div class="flex flex-col text-center w-full mt-5">
               <p class="text-4xl font-bold text-black lg:w-2/3 mx-auto leading-relaxed">Services We Offer
                    <br><span class="inline-block h-1 w-40 rounded bg-blue-900 mt-8 mb-6"></span>
               </p>
          </div>
          <section class="text-gray-900 body-font">
               <div class="container px-5 mt-6 mx-auto">
                    <div class="flex flex-col text-center w-full mb-10">
                         <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Power Supply</h1>
                         <p class="lg:w-2/3 mx-auto leading-relaxed text-md">
                              We Provide
                              <span class="text-2xl text-blue-600">Industrial Equiments</span> and <span
                                   class="text-2xl text-blue-600">24v Industrial Power Supply Products</span>
                              with
                              <span class="text-2xl text-blue-600">Long Term Service</span> with which we have builed a
                              perfect trust across our suppliers<br>
                         </p>
                    </div>
               </div>
               <div class="container px-5 mx-auto">
                    <div class="flex flex-col text-center w-full mb-10">
                         <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Industrial
                              Equipments
                         </h1>
                         <p class="lg:w-2/3 mx-auto leading-relaxed text-md"> We Provide
                              <span class="text-2xl text-blue-600">Industrial Equiments</span>,
                              <span class="text-2xl text-blue-600">Industrial PCB's</span>,
                              <span class="text-2xl text-blue-600">Hybrid Modules</span> and
                              <span class="text-2xl text-blue-600">Motor Drivers</span>
                         </p>
                    </div>
               </div>
               <div class="container px-5 mx-auto">
                    <div class="flex flex-col text-center w-full mb-10">
                         <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">UPS</h1>
                         <p class="lg:w-2/3 mx-auto leading-relaxed text-md">
                              We Provide best in class
                              <span class="text-2xl text-blue-600">Renewable Energies, Solar charger Control for UPS,
                                   Windmill Drivers</span> and <span class="text-2xl text-blue-600">Reverse-Engineering
                                   Drivers</span>
                              all india with a long term service
                         </p>
                    </div>
               </div>
          </section>
     </div>
</div>

<div class="flex flex-col text-center w-full mt-5">
     <p class="text-4xl font-bold text-black lg:w-2/3 mx-auto leading-relaxed">Our Clients
          <br><span class="inline-block h-1 w-32 rounded bg-blue-900 mt-8 mb-6"></span>
     </p>
</div>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="m-5 text-gray-900 body-font">
          <div class="container mx-auto flex px-5  md:flex-row flex-col items-center">
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="object-cover object-center rounded" alt="hero" src="{{ asset('images/TNP.gif') }}">
               </div>
               <div
                    class="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">TAMIL NADU Police
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold"><span class="text-2xl text-blue-600">Tamil Nadu police
                         </span>are one of main clients, were we have done many trust worthy projects.
                         They have gained our best in class communication gadgets
                         which helped them to communicate with the surronding Police Officers.
                         when it comes to communication products Harmonic System delivers best in class
                         Communication gadgets
                    </p>
               </div>
          </div>
     </section>
</div><br>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="m-5 text-gray-900 body-font">
          <div class="container mx-auto flex px-5 md:flex-row flex-col items-center">
               <div class="lg:flex-grow md:w-1/2 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Southern Railway
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold"><span class="text-2xl text-blue-600">Southern Railway
                         </span> and Harmonic Systems are in
                         good relationship were we have done many projects to improve the
                         Southern Railway department by helping them to over come many solution in Power Supply,
                         Commmunication
                         and on industrial Products
                    </p>
               </div>
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="w-auto mx-auto object-cover object-center rounded" alt="hero"
                         src="{{ asset('images/sr.jpg') }}">
               </div>
          </div>
     </section>
</div><br>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="m-5 text-gray-900 body-font">
          <div class="container mx-auto flex px-5 md:flex-row flex-col items-center">
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="w-auto mx-auto object-cover object-center rounded" alt="hero"
                         src="{{ asset('images/vlink.png') }}">
               </div>
               <div class="lg:flex-grow md:w-1/2 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">V-Link
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold">We have done many class level projects to <span
                              class="text-2xl text-blue-600">V-Link
                         </span>
                         were they have gained many solutions to their problems V-link are one of the best cilents we
                         have
                         encountered before
                    </p>
               </div>
          </div>
     </section>
</div><br>
@endsection
@section('footerSection')
<script>
$(" #homepage").addClass("text-gray-900 bg-gray-200");
</script>
@endsection