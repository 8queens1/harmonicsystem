@extends('components.website.layout.app')
@section('headSection')
@endsection
@section('main-content')


<div class="flex flex-col text-center w-full mt-5">
     <p class="text-3xl font-semibold text-black lg:w-2/3 mx-auto leading-relaxed">Why Us
          <br><span class="inline-block h-1 w-20 rounded bg-indigo-500 mt-8 mb-6"></span>
     </p>
</div>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="text-gray-900 body-font">
          <div class="container mx-auto flex px-5 py-4 md:flex-row flex-col items-center">
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="object-cover object-center rounded" alt="hero"
                         src="{{ asset('images/undraw_segment_analysis_bdn4.svg') }}">
               </div>
               <div
                    class="lg:flex-grow md:w-1/2 lg:pl-20 md:pl-11 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">1. The Only One
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold">Harmonic System are the only one to produce <span
                              class="text-2xl text-blue-600">Linear Power Supply
                         </span>across india. Harmonic System provide best in class products for their clients
                         when it comes to quality and service we are the one to provide those services to our clients
                         that's why we are the only one
                    </p>
               </div>
          </div>
     </section>
</div>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="text-gray-900 body-font">
          <div class="container mx-auto flex px-5 py-4 md:flex-row flex-col items-center">
               <div class="lg:flex-grow md:w-1/2 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">2. Best In Services
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold">Harmonic System provide best in services all over
                         india
                         with happy client ends with almost 1000's of products delivered over these years. Harmonic
                         System
                         provide
                         best in class service to our clients, Harmonic System are the only one to provide those service
                         in
                         this
                         type of field all over india
                    </p>
               </div>
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="object-cover object-center rounded" alt="hero"
                         src="{{ asset('images/undraw_agreement_aajr.svg') }}">
               </div>
          </div>
     </section>
</div>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="text-gray-900 body-font">
          <div class="container mx-auto flex px-5 py-4 md:flex-row flex-col items-center">
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="object-cover object-center rounded" alt="hero"
                         src="{{ asset('images/undraw_business_deal_cpi9.svg') }}">
               </div>
               <div
                    class="lg:flex-grow md:w-1/2 lg:pl-20 md:pl-11 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">3. Trust Across Suppliers
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold">Harmonic System have created a great platform where we
                         have
                         gained trust among all the suppliers in these 21years still we look forward to those kind trust
                         rather than
                         going with our sales, trust is something that is more precious than our sales in this field
                    </p>
               </div>
          </div>
     </section>
</div>
<div class="w-11/12 mx-auto md:max-w-screen">
     <section class="text-gray-900 body-font">
          <div class="container mx-auto flex px-5 py-4 md:flex-row flex-col items-center">
               <div class="lg:flex-grow md:w-1/2 flex flex-col md:items-start md:text-left items-center text-center">
                    <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">4. 21Years of Experience
                    </h1>
                    <p class="mb-8 leading-relaxed font-semibold">Harmonic System has over 21years of experince over the
                         fields
                         of power
                         supply
                         communication and on industrial products with experienced people and best in class service we
                         have
                         done
                         our projects totally with the clients requirments, over the past 20years we have gained more
                         happy
                         clients all
                         over the country
                    </p>
               </div>
               <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
                    <img class="object-cover object-center rounded" alt="hero"
                         src="{{ asset('images/undraw_certificate_343v.svg') }}">
               </div>
          </div>
     </section>
</div>

@endsection
@section('footerSection')
<script>
$("#whyus").addClass("text-gray-900 bg-gray-200");
</script>
@endsection