@extends('components.website.layout.app')
@section('headSection')
@endsection
@section('main-content')


<div class="mt-5 sm:px-6 lg:px-8">
     <div
          class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
          <h1>About Us</h1>
     </div>
     <div class="w-11/12 mx-auto overflow-hidden flex flex-col justify-center md:max-w-screen">
          <div class="mt-4">
               <div
                    class="px-6 py-4 rounded-xl text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700  font-bold uppercase">
                    ➼ Harmonic Systems
               </div>
               <div class="bg-white rounded-lg p-8 flex flex-col text-center text-lg w-full">
                    <p class="mx-auto leading-relaxed font-semibold">The Company was incorporated in Aug.
                         1998 It was sister concern
                         of Harmonic Technologies Incorporated in 2000. The other Companies in
                         the group of Harmonic Data Net's.
                         Harmonic has been manufacturing high performance
                         communication power supply's products. Our expertise in this specialised
                         field is unrivalled and has made us the largest products of Communication
                         accessories In 2002 we commenced Service Network of our product at
                         Bangalore.
                         Our power supply have found acceptability by a wide spectrum of
                         user in Government and Industry<br>
                         <span class="text-xl text-blue-600">Manufacturing of Power Electronics & Communication
                              Equipments</span>

                    </p>
               </div>
          </div>
     </div><br>
     <div class="w-11/12 mx-auto md:max-w-screen">
          <div
               class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
               <h1>➼ Harmonic Technologies</h1>
          </div>
          <div class="bg-white rounded-lg p-8 flex flex-col text-center text-lg w-full">
               <p class="mx-auto leading-relaxed font-semibold">The Company was incorporated in 2000.
                    The main goal is to Provide Best in class Service & Maintenance all over India and extend our happy
                    <span class="text-xl text-blue-600">Customers</span>.<br>
                    <span class="text-xl text-blue-600">Service & Maintenance</span>
               </p>
          </div>
     </div><br>
     <div class="w-11/12 mx-auto md:max-w-screen">
          <div
               class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
               <h1>➼ Harmonic Data Nets</h1>
          </div>
          <div class="bg-white rounded-lg p-8 flex flex-col text-center text-lg w-full">
               <p class="mx-auto leading-relaxed font-semibold">The Company was incorporated Along with Harmonic
                    Technologies.
                    Harmonic Data Nets manufacturer
                    <span class="text-xl text-blue-600">LAN/WAN Network Accessories</span><br>
               </p><br>
               <div class="w-1/2 mx-auto md:max-w-screen">
                    <div
                         class="px-6 py-4 text-white bg-gradient-to-r from-blue-900 via-blue-700 to-blue-700 font-bold uppercase rounded-xl">
                         <h1>Manufacturing of Network Accessories </h1>
                    </div>
                    <p class="bg-white rounded-xl mb-8 leading-relaxed font-semibold">
                         ➼ Jack Panel<br>
                         ➼ Cable Manager<br>
                         ➼ Power Manager<br>
                         ➼ 19" Racks
                    </p>
               </div>
          </div>
     </div>

</div><br>
@endsection
@section('footerSection')
<script>
$("#aboutus").addClass("text-gray-900 bg-gray-200");
</script>
@endsection