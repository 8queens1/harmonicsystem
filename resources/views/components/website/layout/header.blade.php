<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title')</title>

<link rel="icon" type="image/jpeg" href="{{ asset('images/HLOGO.png') }}">
<meta name="msapplication-TileColor" content="{{ App::make('configuration')->theme_color }}">
<meta name="msapplication-TileImage" content="@yield('image')">
<meta name="theme-color" content="{{ App::make('configuration')->theme_color }}">
<meta name="robots" content="index, follow" />
<meta name="description" content="@yield('description')" />
<meta name="Keywords" content="@yield('keyword')" />
<meta property="og:title" content="@yield('title')" />
<meta property="og:type" content="website" />
<link href="{{ Request::url() }}" rel="canonical">
<link href="{{ App::make('configuration')->website }}" rel="home">
<meta name="twitter:site" content="{{ App::make('configuration')->company_name }}">
<meta name="twitter:creator" content="{{ App::make('configuration')->company_name }}">
<meta name="twitter:title" content="@yield('title')">
<meta name="twitter:description" content="@yield('description')">
<meta property="og:description" content="@yield('description')" />
<meta property="og:url" content="{{ Request::url() }}" />
<meta property="og:site_name" content={{ App::make('configuration')->website }} />
<meta property="og:image" content="@yield('image')" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="author" content="{{ App::make('configuration')->company_name }}">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">


</head>