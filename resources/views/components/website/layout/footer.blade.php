<br>
<footer class="bg-white">
     <div
          class="container px-5 py-24 mx-auto flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
          <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
               <a class="flex title-font font-medium items-center md:justify-start justify-center text-gray-900">
                    <img class="w-auto h-11 mx-auto" alt="hero" src="{{ asset('images/HLOGO.png') }}">
                    <img class="m-3 w-auto h-8 " alt="hero" src="{{ asset('images/Hname.png') }}">
               </a>
          </div>
          <div class="flex-grow flex flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center">
               <div class="lg:w-1/4 md:w-1/2 w-full px-4">
                    <h2 class="font-semibold text-xl text-gray-900 tracking-widest mb-3">CHENNAI BRANCH</h2>
                    <nav class="text-gray-900 font-sm list-none mb-10">
                         #6, Vellalar Street, 3rd Main Road. Ambattur Industrial Estate,
                         chennai-58 Mobile: 9840734558, 9840105883
                         Fax: 91-044-26245975
                    </nav>
               </div>
               <div class="lg:w-1/4 md:w-1/2 w-full px-4">
                    <h2 class="font-semibold text-xl text-gray-900 tracking-widest mb-3">BANGALORE BRANCH</h2>
                    <nav class="text-gray-900 font-sm list-none mb-10">
                         #C71, East First Lane, I.T.I. TownShip
                         Doorvani Nagar, Bangalore-560 016
                         Mobile : 9845131604
                    </nav>
               </div>
               <div class="lg:w-1/4 md:w-1/2 w-full px-4">
                    <h2 class="font-semibold text-xl text-gray-900 tracking-widest mb-3">CLIENT SUPPORT</h2>
                    <nav class="list-none mb-10">
                         <li>
                              <a class="text-gray-900 font-sm">Contact Us</a>
                         </li>
                         <li>
                              <a class="text-gray-900 font-sm">Products</a>
                         </li>
                    </nav>
               </div>
               <div class="lg:w-1/4 md:w-1/2 w-full px-4">
                    <h2 class="font-semibold text-xl text-gray-900 tracking-widest mb-3">BUSINESS TIMING</h2>
                    <nav class="text-gray-900 font-sm ">
                         MON-SAT 9AM-7PM<br>
                         SUN-CLOSED
                    </nav>
               </div>
          </div>
     </div>
     <div class="bg-gray-100">
          <div class="container mx-auto py-4 px-5 flex flex-wrap flex-col sm:flex-row">
               <p class="font-semibold text-gray-500 text-sm text-center sm:text-left">© 2021 8Queens —
                    <a href="https://twitter.com/knyttneve" rel="noopener noreferrer" class="text-gray-600 ml-1"
                         target="_blank">All Rights Reserved @HarmonicSystem</a>
               </p>
               <span class="inline-flex sm:ml-auto sm:mt-0 mt-2 justify-center sm:justify-start">
                    <a class="text-gray-500">
                         <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              class="w-5 h-5" viewBox="0 0 24 24">
                              <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                         </svg>
                    </a>
                    <a class="ml-3 text-gray-500">
                         <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              class="w-5 h-5" viewBox="0 0 24 24">
                              <path
                                   d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                              </path>
                         </svg>
                    </a>
                    <a class="ml-3 text-gray-500">
                         <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                              stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                              <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
                              <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                         </svg>
                    </a>
                    <a class="ml-3 text-gray-500">
                         <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                              stroke-width="0" class="w-5 h-5" viewBox="0 0 24 24">
                              <path stroke="none"
                                   d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z">
                              </path>
                              <circle cx="4" cy="4" r="2" stroke="none"></circle>
                         </svg>
                    </a>
               </span>
          </div>
     </div>
</footer>























<!-- @if(App::make('configuration')->mailchimpflag)
<div class="container mx-auto font-sans bg-gradient-to-r {{ str_replace(['bg', 500],['from', 500], App::make('configuration')->theme_color ) }} {{ str_replace(['bg', 500],['from', 200], App::make('configuration')->theme_color ) }}  rounded-lg shadow hover:shadow-lg mt-8 mb-8  lg:p-20 p-10 text-center">
    <h2 class="font-bold break-normal text-gray-800 text-2xl md:text-4xl">Subscribe to OrgoStore Blog</h2>
    <h3 class="font-bold break-normal text-gray-600 text-base md:text-xl">Get the latest posts delivered right to your inbox</h3>
    <div class="w-full text-center pt-4">
       <form action="{{ url('newsletter') }}" method="post" class="form_prevent_multiple_submits">
          @csrf
          <div class="max-w-xl mx-auto p-1 pr-0 flex flex-wrap items-center">
          <input type="email" name="mail" value="{{ old("mail") }}" placeholder="youremail@example.com" class="flex-1 appearance-none rounded shadow p-3 text-gray-600 mr-2 focus:outline-none">
          <i class="fa fa-spinner m-1 text-gray-900 fa-spin"></i><button type="submit" class="button_prevent_multiple_submits flex-1 mt-4 md:mt-0 block md:inline-block appearance-none {{ App::make('configuration')->theme_color }} {{ App::make('configuration')->text_color }} text-base font-semibold tracking-wider uppercase py-4 rounded shadow hover:bg-yellow-500"> Subscribe</button>
          </div>
          <span class="text-left"> @include('helper.formerror', ['error' => "mail"]) </span>
       </form>
    </div>
 </div>
@endif


<div class="container mx-auto p-4 shadow">
 {{ App::make('configuration')->googleadshorizontalcode }}
</div>

<div class="{{ App::make('configuration')->footerbg_color }}">
    <div class="max-w-6xl m-auto {{ App::make('configuration')->footertext_color }} flex flex-wrap justify-center">
       <div class="p-5 w-96 ">

        <a href="{{ URL('/') }}">
         @if(App::make('configuration')->uplode_logo_image)
         <img width="110" src="{{ URL('/image/'.App::make('configuration')->uplode_logo_image) }}" class="p-8 ml-2" alt="{{ App::make('configuration')->company_name }}">
         @else
         <h1 class="text-2xl p-8 ml-2 {{ App::make('configuration')->headertext_color }}"> {{ App::make('configuration')->company_name }} </h1>
         @endif
        </a>

        <div class="md:flex-auto mt-2 flex-row flex">

         @if(App::make('configuration')->twitter)
            <a href="{{ App::make('configuration')->twitter }}" target="_blank"  class="w-12 mx-1">
               <svg class="fill-current cursor-pointer {{ str_replace("bg","text", App::make('configuration')->theme_color ) }} hover:{{ str_replace(['bg', 500],['text', 400], App::make('configuration')->theme_color ) }}" width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 2;">
                  <path id="Twitter" d="M24,12c0,6.627 -5.373,12 -12,12c-6.627,0 -12,-5.373 -12,-12c0,-6.627
                     5.373,-12 12,-12c6.627,0 12,5.373 12,12Zm-6.465,-3.192c-0.379,0.168
                     -0.786,0.281 -1.213,0.333c0.436,-0.262 0.771,-0.676
                     0.929,-1.169c-0.408,0.242 -0.86,0.418 -1.341,0.513c-0.385,-0.411
                     -0.934,-0.667 -1.541,-0.667c-1.167,0 -2.112,0.945 -2.112,2.111c0,0.166
                     0.018,0.327 0.054,0.482c-1.754,-0.088 -3.31,-0.929
                     -4.352,-2.206c-0.181,0.311 -0.286,0.674 -0.286,1.061c0,0.733 0.373,1.379
                     0.94,1.757c-0.346,-0.01 -0.672,-0.106 -0.956,-0.264c-0.001,0.009
                     -0.001,0.018 -0.001,0.027c0,1.023 0.728,1.877 1.694,2.07c-0.177,0.049
                     -0.364,0.075 -0.556,0.075c-0.137,0 -0.269,-0.014 -0.397,-0.038c0.268,0.838
                     1.048,1.449 1.972,1.466c-0.723,0.566 -1.633,0.904 -2.622,0.904c-0.171,0
                     -0.339,-0.01 -0.504,-0.03c0.934,0.599 2.044,0.949 3.237,0.949c3.883,0
                     6.007,-3.217 6.007,-6.008c0,-0.091 -0.002,-0.183 -0.006,-0.273c0.413,-0.298
                     0.771,-0.67 1.054,-1.093Z"></path>
               </svg>
            </a>
            @endif

            @if(App::make('configuration')->facebook)
            <a href="{{ App::make('configuration')->facebook }}" target="_blank"  class="w-12 mx-1">
               <svg class="fill-current cursor-pointer {{ str_replace("bg","text", App::make('configuration')->theme_color ) }} hover:{{ str_replace(['bg', 500],['text', 400], App::make('configuration')->theme_color ) }}" width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 2;">
                  <path id="Facebook" d="M24,12c0,6.627 -5.373,12 -12,12c-6.627,0 -12,-5.373 -12,-12c0,-6.627
                     5.373,-12 12,-12c6.627,0 12,5.373
                     12,12Zm-11.278,0l1.294,0l0.172,-1.617l-1.466,0l0.002,-0.808c0,-0.422
                     0.04,-0.648 0.646,-0.648l0.809,0l0,-1.616l-1.295,0c-1.555,0 -2.103,0.784
                     -2.103,2.102l0,0.97l-0.969,0l0,1.617l0.969,0l0,4.689l1.941,0l0,-4.689Z"></path>
               </svg>
            </a>
            @endif

            @if(App::make('configuration')->youtube)
            <a href="{{ App::make('configuration')->youtube }}" target="_blank"  class="w-12 mx-1">
               <svg class="fill-current cursor-pointer {{ str_replace("bg","text", App::make('configuration')->theme_color ) }} hover:{{ str_replace(['bg', 500],['text', 400], App::make('configuration')->theme_color ) }}" width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 2;">
                  <g id="Layer_1">
                     <circle id="Oval" cx="12" cy="12" r="12"></circle>
                     <path id="Shape" d="M19.05,8.362c0,-0.062 0,-0.125 -0.063,-0.187l0,-0.063c-0.187,-0.562
                        -0.687,-0.937 -1.312,-0.937l0.125,0c0,0 -2.438,-0.375 -5.75,-0.375c-3.25,0
                        -5.75,0.375 -5.75,0.375l0.125,0c-0.625,0 -1.125,0.375
                        -1.313,0.937l0,0.063c0,0.062 0,0.125 -0.062,0.187c-0.063,0.625 -0.25,1.938
                        -0.25,3.438c0,1.5 0.187,2.812 0.25,3.437c0,0.063 0,0.125
                        0.062,0.188l0,0.062c0.188,0.563 0.688,0.938 1.313,0.938l-0.125,0c0,0
                        2.437,0.375 5.75,0.375c3.25,0 5.75,-0.375 5.75,-0.375l-0.125,0c0.625,0
                        1.125,-0.375 1.312,-0.938l0,-0.062c0,-0.063 0,-0.125
                        0.063,-0.188c0.062,-0.625 0.25,-1.937 0.25,-3.437c0,-1.5 -0.125,-2.813
                        -0.25,-3.438Zm-4.634,3.927l-3.201,2.315c-0.068,0.068 -0.137,0.068
                        -0.205,0.068c-0.068,0 -0.136,0 -0.204,-0.068c-0.136,-0.068 -0.204,-0.204
                        -0.204,-0.34l0,-4.631c0,-0.136 0.068,-0.273 0.204,-0.341c0.136,-0.068
                        0.272,-0.068 0.409,0l3.201,2.316c0.068,0.068 0.136,0.204
                        0.136,0.34c0.068,0.136 0,0.273 -0.136,0.341Z" style="fill: rgb(255, 255, 255);"></path>
                  </g>
               </svg>
            </a>
            @endif

            @if(App::make('configuration')->instagram)
            <a href="{{ App::make('configuration')->instagram }}" target="_blank"  class="w-12 mx-1">
               <svg xmlns="http://www.w3.org/2000/svg" class="fill-current cursor-pointer {{ str_replace("bg","text", App::make('configuration')->theme_color ) }} hover:{{ str_replace(['bg', 500],['text', 400], App::make('configuration')->theme_color ) }}" width="100%" height="100%" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 7.082c1.602 0 1.792.006 2.425.035 1.627.074 2.385.845 2.46 2.459.028.633.034.822.034 2.424s-.006 1.792-.034 2.424c-.075 1.613-.832 2.386-2.46 2.46-.633.028-.822.035-2.425.035-1.602 0-1.792-.006-2.424-.035-1.63-.075-2.385-.849-2.46-2.46-.028-.632-.035-.822-.035-2.424s.007-1.792.035-2.424c.074-1.615.832-2.386 2.46-2.46.632-.029.822-.034 2.424-.034zm0-1.082c-1.63 0-1.833.007-2.474.037-2.18.1-3.39 1.309-3.49 3.489-.029.641-.036.845-.036 2.474 0 1.63.007 1.834.036 2.474.1 2.179 1.31 3.39 3.49 3.49.641.029.844.036 2.474.036 1.63 0 1.834-.007 2.475-.036 2.176-.1 3.391-1.309 3.489-3.49.029-.64.036-.844.036-2.474 0-1.629-.007-1.833-.036-2.474-.098-2.177-1.309-3.39-3.489-3.489-.641-.03-.845-.037-2.475-.037zm0 2.919c-1.701 0-3.081 1.379-3.081 3.081s1.38 3.081 3.081 3.081 3.081-1.379 3.081-3.081c0-1.701-1.38-3.081-3.081-3.081zm0 5.081c-1.105 0-2-.895-2-2 0-1.104.895-2 2-2 1.104 0 2.001.895 2.001 2s-.897 2-2.001 2zm3.202-5.922c-.397 0-.72.322-.72.72 0 .397.322.72.72.72.398 0 .721-.322.721-.72 0-.398-.322-.72-.721-.72z"/></svg>
            </a>
            @endif


            @if(App::make('configuration')->linkedin)
            <a href="{{ App::make('configuration')->linkedin }}" target="_blank"  class="w-12 mx-1">
               <svg class="fill-current cursor-pointer {{ str_replace("bg","text", App::make('configuration')->theme_color ) }} hover:{{ str_replace(['bg', 500],['text', 400], App::make('configuration')->theme_color ) }}" width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 2;">
                  <path id="Shape" d="M7.3,0.9c1.5,-0.6 3.1,-0.9 4.7,-0.9c1.6,0 3.2,0.3 4.7,0.9c1.5,0.6 2.8,1.5
                     3.8,2.6c1,1.1 1.9,2.3 2.6,3.8c0.7,1.5 0.9,3 0.9,4.7c0,1.7 -0.3,3.2
                     -0.9,4.7c-0.6,1.5 -1.5,2.8 -2.6,3.8c-1.1,1 -2.3,1.9 -3.8,2.6c-1.5,0.7
                     -3.1,0.9 -4.7,0.9c-1.6,0 -3.2,-0.3 -4.7,-0.9c-1.5,-0.6 -2.8,-1.5
                     -3.8,-2.6c-1,-1.1 -1.9,-2.3 -2.6,-3.8c-0.7,-1.5 -0.9,-3.1 -0.9,-4.7c0,-1.6
                     0.3,-3.2 0.9,-4.7c0.6,-1.5 1.5,-2.8 2.6,-3.8c1.1,-1 2.3,-1.9
                     3.8,-2.6Zm-0.3,7.1c0.6,0 1.1,-0.2 1.5,-0.5c0.4,-0.3 0.5,-0.8 0.5,-1.3c0,-0.5
                     -0.2,-0.9 -0.6,-1.2c-0.4,-0.3 -0.8,-0.5 -1.4,-0.5c-0.6,0 -1.1,0.2
                     -1.4,0.5c-0.3,0.3 -0.6,0.7 -0.6,1.2c0,0.5 0.2,0.9 0.5,1.3c0.3,0.4 0.9,0.5
                     1.5,0.5Zm1.5,10l0,-8.5l-3,0l0,8.5l3,0Zm11,0l0,-4.5c0,-1.4 -0.3,-2.5
                     -0.9,-3.3c-0.6,-0.8 -1.5,-1.2 -2.6,-1.2c-0.6,0 -1.1,0.2 -1.5,0.5c-0.4,0.3
                     -0.8,0.8 -0.9,1.3l-0.1,-1.3l-3,0l0.1,2l0,6.5l3,0l0,-4.5c0,-0.6 0.1,-1.1
                     0.4,-1.5c0.3,-0.4 0.6,-0.5 1.1,-0.5c0.5,0 0.9,0.2 1.1,0.5c0.2,0.3 0.4,0.8
                     0.4,1.5l0,4.5l2.9,0Z"></path>
               </svg>
            </a>
            @endif
         </div>

       </div>
       <div class="p-5 w-48 ">
          <div class="text-xs uppercase {{ App::make('configuration')->footertext_color }} font-medium">Category</div>
          @if(App\Models\Admin\Blog\Category::where('status', 1)->count())
          @foreach(App\Models\Admin\Blog\Category::where('status', 1)->inRandomOrder()->take(10)->get() as $eachCategories)
          <a class="my-3 block" href="{{ route('category', [$eachCategories->id]) }}">{{ $eachCategories->name }} <span class="text-teal-600 text-xs p-1"></span></a>
          @endforeach
          @endif


       </div>
       <div class="p-5 w-48 ">
         @if(App\Models\Admin\Website\Websitepages::where('active', 1)->count())
         <div class="text-xs uppercase {{ App::make('configuration')->footertext_color }} font-medium">Pages</div>
            @foreach (App\Models\Admin\Website\Websitepages::where('active', 1)->get() as $key => $list)
            <a class="my-3 block {{ App::make('configuration')->footertext_color }}" target="_blank" href="{{ URL('/page/'. $list->slug) }}">{{ $list->name }}</a>
         @endforeach
         @endif
       </div>

       {{-- bg-gradient-to-r from-green-500 to-green-300 --}}

       <div class="p-5 w-96 hover:shadow-lg shadow mt-6 bg-gray-400 rounded-lg">
        <form class="w-full max-w-lg form_prevent_multiple_submits" id="my-form-id" method="POST" action="{{ url('contactus') }}">
           @csrf
            <div class="flex flex-wrap -mx-3 mb-4">
              <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-first-name">
                  First Name
                </label>
                <input class="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name"
                type="text" placeholder="Jane" name="fname" value="{{ old("fname") }}">
                {{-- <span class="text-red-500 text-xs italic"> @include('helper.formerror', ['error' => "fname"]) </span> --}}
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-last-name">
                  Last Name
                </label>
                <input class="appearance-none block w-full bg-gray-100 text-gray-700 border border-gray-100 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name"
                type="text" placeholder="Doe" name="lname" value="{{ old("lname") }}">
                {{-- <span class="text-red-500 text-xs italic"> @include('helper.formerror', ['error' => "lname"]) </span> --}}
              </div>
              <span class="text-red-500 text-xs italic ml-2"> @include('helper.formerror', ['error' => "fname"]) </span>
            </div>
            <div class="flex flex-wrap -mx-3 mb-3">
              <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-password">
                  E-mail
                </label>
                <input class="appearance-none block w-full bg-gray-100 text-gray-700 border border-gray-100 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="email" type="email" name="email" value="{{ old("email") }}">
                <span class="text-red-500 text-xs italic"> @include('helper.formerror', ['error' => "email"]) </span>
              </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-3">
              <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-900 text-xs font-bold mb-2" for="grid-password">
                  Message
                </label>
                <textarea class=" no-resize appearance-none block w-full bg-gray-100 text-gray-700 border border-gray-100 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-16 resize-none"
                id="message" name="message">{{ old("message") }}</textarea>
                <span class="text-red-500 text-xs italic"> @include('helper.formerror', ['error' => "message"]) </span>
              </div>
            </div>

          @if(App::make('configuration')->recaptchasecretstatus)
          <span class="text-red-500 text-xs italic"> @include('helper.formerror', ['error' => "g-recaptcha-response"]) </span>
          <div class="md:flex md:items-center">
            <div class="md:w-1/3">
               <i class="fa fa-spinner m-1 text-gray-900 fa-spin float-right"></i>
            </div>
          <div class="md:w-1/3">
           {!! NoCaptcha::displaySubmit('my-form-id', '<i class="fa fa-spinner m-1 bg-white fa-spin"></i> Send', ['data-theme' => 'light', 'class' => 'button_prevent_multiple_submits shadow bg-yellow-500 focus:shadow-outline focus:outline-none text-gray-900 font-bold py-2 px-4 rounded']) !!}
         </div>
         <div class="md:w-1/3"></div>
       </div>
            @else
            <div class="md:flex md:items-center">
               <div class="md:w-1/3">
                  <i class="fa fa-spinner m-1 text-gray-900 fa-spin float-right"></i>
               </div>
             <div class="md:w-1/3">
               <button class="button_prevent_multiple_submits shadow bg-yellow-500 focus:shadow-outline focus:outline-none text-gray-900 font-bold py-2 px-4 rounded" type="submit">
                   Send
               </button>
             </div>
             <div class="md:w-1/3"></div>
           </div>
            @endif
          </form>
       </div>
    </div>
 </div>

 <div class="{{ App::make('configuration')->footerbg_color }} pt-2 ">
    <div class="flex pb-5 px-3 m-auto pt-5 border-t {{ App::make('configuration')->footertext_color }} text-sm flex-col
       md:flex-row max-w-6xl">
       <div class="mt-2">{{ App::make('configuration')->copyrights }}</div>
    </div>
 </div> -->