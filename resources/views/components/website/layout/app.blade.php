<!DOCTYPE html>
<html lang="en">

<head>
     <x-website.layout.header />
     <x-website.layout.headerlibrary />
</head>

<body class="bg-indigo-100">
     @include('sweetalert::alert')
     <x-website.layout.navbar />
     @section('main-content')
     @show
     <x-website.layout.footer />
     <x-website.layout.footerlibrary />
</body>

</html>