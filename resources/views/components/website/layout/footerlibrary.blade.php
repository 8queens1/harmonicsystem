<!-- jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<link href="{{ asset('youtube/video-js.css') }}" rel="stylesheet">
<script src="{{ asset('youtube/video.min.js') }}"></script>
<script src="{{ asset('youtube/youtube.min.js') }}"></script>
<!-- Font Awesome -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>

<style type="text/css">
  .fa-spin {
       display: none;
  }
</style>

  <script type="text/javascript">
     (function () {
         $('.form_prevent_multiple_submits').on('submit', function(){
            $('.button_prevent_multiple_submits').attr('disabled', 'true');
            $('.fa-spin').show();
         })
     })();
  </script>



<script type="text/javascript">
  $(document).ready(function () {
   
      $('#searchlistweb').on('keyup',function() {
          var query = $(this).val(); 
          $.ajax({
              url:"{{ route('search') }}",
              type:"GET",
              data:{'searchlist':query},
              success:function (data) {
                  $('#searchlist_listweb').html(data);
              }
          })
          // end of ajax call
      });

      
      $(document).on('click', 'li', function(){
          var value = $(this).text();
          $('#searchlistweb').val(value);
          $('#searchlist_listweb').html("");
          $("#searchpostweb").submit();
      });
  });
</script>

<script type="text/javascript">
  $(document).ready(function () {
   
      $('#searchlistmobile').on('keyup',function() {
          var query = $(this).val(); 
          $.ajax({
              url:"{{ route('search') }}",
              type:"GET",
              data:{'searchlist':query},
              success:function (data) {
                  $('#searchlist_listmobile').html(data);
              }
          })
          // end of ajax call
      });

      
      $(document).on('click', 'li', function(){
          var value = $(this).text();
          $('#searchlistmobile').val(value);
          $('#searchlist_listmobile').html("");
          $("#searchpostmobile").submit();
      });
  });
</script>

{!! App::make('configuration')->socialmediaicon !!}
{!! App::make('configuration')->googleanalyticscode !!}
  @section('footerSection')
 @show