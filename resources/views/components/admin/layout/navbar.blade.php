<header  class="w-full flex items-center bg-white py-2 px-6 hidden sm:flex">
    <div class="w-1/2">
        <a href="#" @click=" isSidenavOpen = !isSidenavOpen">
            <i class="fas fa-bars fa-2x mr-3 bg-grey"></i>
        </a>
    </div>
    <div x-data="{ isOpen: false }" class="relative w-1/2 flex justify-end">
        <button @click="isOpen = !isOpen" class="realtive z-10 w-12 h-12 rounded-full overflow-hidden border-4 border-gray-400 hover:border-gray-300 focus:border-gray-300 focus:outline-none">
            
            @if(Auth::user()->avatar)
            <img src="{{ URL('/avatar/thumbnail/'.  Auth::user()->avatar) }}">
            @else
            <img src="{{ URL('/avatar/profile.jpeg') }}">
            @endif
        </button>
        <button x-show="isOpen" @click="isOpen = false" class="h-full w-full fixed inset-0 cursor-default"></button>
        <div x-show="isOpen" class="absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-12">
            <a href="{{ route('2fa') }}" class="block px-4 py-2 account-link hover:text-white">2FA</a>
            <a class="block px-4 py-2 account-link hover:text-white" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</header>

