<ul class="flex justify-between m-3">
    <li class="mr-3 lg:text-xl text-xl">
        @if ($obj->id)
        <i class="fas fa-list mr-3"></i>UPDATE {{ $title }}
        @else
        <i class="fas fa-list mr-3"></i>ADD NEW {{ $title }}
        @endif
    </li>
    <li class="mr-3">
        @if($backbutton == 'enable')
       <a class="inline-block border border-gray-500 rounded py-1 px-4 bg-gray-600 hover:bg-gray-700 text-white" href="{{route($name.'.index')}}">Back</a>
       @elseif ($backbutton == 'settings')
       <a class="inline-block border border-gray-500 rounded py-1 px-4 bg-gray-600 hover:bg-gray-700 text-white" href="{{route('settings')}}">Back</a>
       @endif

    </li>
 </ul>