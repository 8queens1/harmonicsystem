<aside x-show="isSidenavOpen"  class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl">
   <div class="p-5">
      <a href="{{ route('dashboard') }}" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
   </div>
   <nav  class="text-white text-base font-semibold pt-3">
      <a href="{{ route('dashboard') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
      <i class="fas fa-tachometer-alt mr-3"></i>
      Dashboard
      </a>
      <div x-data="{ isBlogOpen: false }">
         <a @click=" isBlogOpen = !isBlogOpen" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
         <i class="fas fa-sticky-note mr-3"></i>
         <span> Blog </span>
         <span class="ml-10">
         <i x-show="!isBlogOpen" class="fas fa-angle-double-right float-right"></i>
         <i x-show="isBlogOpen" class="fas fa-angle-double-down float-right"></i>    
         </span>
         </a>
         <div x-show="isBlogOpen" class="flex flex-col bg-blue-800 text-blue-300">
            <a href="{{ route('post.index') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">Post</span></a>
            <a href="{{ route('category.index') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">Category</span></a>
            <a href="{{ route('subcategory.index') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">Sub Category</span></a>
            <a href="{{ route('tag.index') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">Tag</span></a>
            <a href="{{ route('aboutme.index') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">About Me</span></a>
         </div>
      </div>
      <a href="{{ route('newsletteruser.index') }}" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
      <i class="fas fa-tablet-alt mr-3"></i>
      Newsletter
      </a>
      <a href="{{ route('marquee.index') }}" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
      <i class="fas fa-calendar mr-3"></i>
      Marquee
      </a>
      <a href="{{ route('enquiry.index') }}" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
      <i class="fas fa-calendar mr-3"></i>
      Contact Us
      </a>
      <a href="{{ route('eventcalendar.index') }}" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
      <i class="fas fa-calendar mr-3"></i>
      Calendar
      </a>
      <a href="{{ route('websitepages.index') }}" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
      <i class="fas fa-calendar mr-3"></i>
      Pages
      </a>
      <a href="{{ route('settings') }}" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
      <i class="fas fa-calendar mr-3"></i>
      Settings
      </a>
      <div x-data="{ isTrackingOpen: false }">
         <a @click=" isTrackingOpen = !isTrackingOpen" class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
         <i class="fas fa-sticky-note mr-3"></i>
         <span> Tracking </span>
         <span class="ml-10">
         <i x-show="!isTrackingOpen" class="fas fa-angle-double-right float-right"></i>
         <i x-show="isTrackingOpen" class="fas fa-angle-double-down float-right"></i>    
         </span>
         </a>
         <div x-show="isTrackingOpen" class="flex flex-col bg-blue-800 text-blue-300">
            <a href="{{ route('loginlogs') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">Login Info</span></a>
            <a href="{{ route('trackinglogs') }}" class="text-sm text-white flex p-3 hover:bg-blue-600"><span class="ml-3">User Activity</span></a>
         </div>
      </div>

   </nav>
   {{-- <a class="absolute w-full upgrade-btn bottom-0 active-nav-link text-white flex items-center justify-center py-4" href="{{ route('logout') }}"
      onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
   <i class="fas fa-sign-out-alt mr-3"></i>
   {{ __('Logout') }}
   </a>
   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
   </form> --}}
</aside>