<ul class="flex justify-between mx-12 mb-3">
    <li class="mr-2 lg:text-2xl text-xl">
        <i class="fas fa-assistive-listening-systems mr-2"></i>{{ $title }}
    </li>
    <li class="mr-2">
        @if($button == 'disable')
        <span></span>
        @elseif($button == 'ajax')
        <a href="javascript:void(0)" class="btn btn-success my-2 my-sm-0" id="{{ 'add_'. $name }}">CREATE</a>
        @elseif($button == 'normal')
        <a class="inline-block border border-blue-500 rounded py-1 px-4 bg-blue-600 hover:bg-blue-700 text-white" href="{{route($name.'.create')}}">Create</a>
        @endif
    </li>
  </ul>