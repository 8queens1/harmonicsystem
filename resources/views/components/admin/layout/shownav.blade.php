<ul class="flex justify-between m-3 text-black">
    <li class="mr-3 lg:text-xl text-xl">
        <i class="fas fa-eye mr-3"></i>{{ $title }}
    </li>
    <li class="mr-3">
        @if($backbutton == 'enable')
       <a class="inline-block border border-gray-500 rounded py-1 px-4 bg-gray-600 hover:bg-gray-700 text-white" href="{{route($name.'.index')}}">Back</a>
       @endif
    </li>
 </ul>