<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <x-admin.layout.header />
    <x-admin.layout.headerlibrary />
</head>
<body  x-data="{ isSidenavOpen: false }" class="bg-gray-100 font-family-karla flex">
    @include('sweetalert::alert')
    <x-admin.layout.sidenav />

    <div class="w-full flex flex-col h-screen overflow-y-hidden">
        <!-- Desktop Header -->
        <x-admin.layout.navbar />

        <!-- Mobile Header & Nav -->
        <x-admin.layout.mobileheaderandnav />
    
        <div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
            @section('main-content')
            @show 
    
            <x-admin.layout.footer />
        </div>
        
    </div>
    <x-admin.layout.footerlibrary />
</body>
</html>