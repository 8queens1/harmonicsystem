module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'orgostore-green':'#52b34dc7',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
