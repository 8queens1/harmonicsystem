<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();

            $table->string('sys_id')->unique();
            $table->string('uniqid')->unique();
            $table->string('uuid')->unique();
            $table->integer('sequence_id');

            $table->string('category')->nullable();
            $table->string('subcategory')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('subcategory_id')->nullable();

            $table->integer('user_id');
            $table->string('created_by');

            $table->string('title', 256)->index();
            $table->string('subtitle', 256)->nullable();
            $table->string('postslug', 256)->unique();
            $table->text('interpretation');
            $table->longText('body');
            $table->boolean('status')->nullable();
            $table->string('image')->nullable();
            $table->boolean('imagestatus')->nullable(); // need for image status
            $table->integer('like')->nullable();
            $table->integer('dislike')->nullable();
            $table->string('keyword')->nullable();
            $table->text('description')->nullable();
            $table->text('comments')->nullable();

            $table->string('video_link')->nullable(); // 0 - Inactive 1 - active
            $table->boolean('video_status')->defalut(0);

            $table->boolean('blogcomment')->defalut(0);
            
            $table->boolean('top')->nullable();

            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();

            $table->boolean('publish')->defalut(0);
            $table->boolean('active')->defalut(0);
            $table->string('flag')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
