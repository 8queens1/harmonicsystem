<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsitepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websitepages', function (Blueprint $table) {
            $table->id();
            $table->string('sys_id')->unique();
            $table->string('uuid')->unique();
            $table->string('uniqid')->unique();
            $table->integer('sequence_id');
            $table->integer('user_id');
            $table->string('created_by');
            $table->string('status');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('keyword')->nullable();
            $table->text('description')->nullable();
            $table->longText('page')->nullable();
            $table->text('comments')->nullable();

            $table->boolean('active')->defalut(0);
            $table->string('flag')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websitepages');
    }
}
