<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutmesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aboutmes', function (Blueprint $table) {
            $table->id();

            $table->string('uuid')->nullable();

            $table->string('title');
            $table->string('avatar')->nullable();
            $table->text('body');
            $table->boolean('active')->default(0);
            $table->boolean('avataractive')->default(0);
            
            $table->string('avatartaglineone')->nullable();
            $table->string('avatartaglinetwo')->nullable();
            
            $table->string('updated_id')->nullable();
            $table->string('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aboutmes');
    }
}
